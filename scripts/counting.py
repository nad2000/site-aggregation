#!/usr/bin/python
# Dummp the data of all shops.

SHOPS = []

'''
# Use parent path

## Method 1, use relative path
I have this directory tree:
  codes
    get_ec_data
      __init__.py
      crawler
        __init__.py
        private_settings.py
      result_visualizer
        __init__.py
        counting.py
        shop_dump.py

I need to call private_settings from counting.py
  [](http://stackoverflow.com/questions/714063/python-importing-modules-from-parent-folder)
  [](https://docs.python.org/2/whatsnew/2.5.html#pep-328-absolute-and-relative-imports)

Suppose that I am in directory 'get_ec_data', add this line in counting.py
  from crawler.private_settings import SHOPS
Then, call callcounting.py [Executing modules as scripts](https://www.python.org/dev/peps/pep-0338/)
  python -m result_visualizer.counting

## Method 2: change current working directory

'''

from crawler.private_settings import SHOPS
assert len(SHOPS)

import subprocess as sp
import re
import os
for shopUrl in SHOPS:
  m = re.search('https://www.bukalapak.com/(.*)', shopUrl)
  assert m, shopUrl
  shopId = m.group(1)
  launchCmd = 'python -m result_visualizer.shop_dump %s --csv data-%s --data likes' % (shopUrl, shopId)
  #process = sp.Popen(launchCmd.split())
  os.system(launchCmd)
