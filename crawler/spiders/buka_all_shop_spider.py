from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from crawler.bukalapak.ec_market_analyzer import EcMarketAnalyzer
from crawler.util import Util
from random import shuffle

class BukaAllShopSpider(CrawlSpider):
  name = 'allShopSpider'
  allowed_domains = ['bukalapak.com']
  #start_urls = [product.url for product in Product.select(Product.url)]

  # 1st and 2nd level categories
  categories = [
     '/c/buk',
     '/c/buku/novel',
     '/c/buku/agama-kepercayaan',
     '/c/buku/komik',
     '/c/buku/anak-anak',
     '/c/buku/pendidikan',
     '/c/buku/lain-lain-506',
     '/c/buku/komputer-487',
     '/c/buku/pengembangan-diri',
     '/c/buku/sosial',
     '/c/buku/bisnis',
     '/c/buku/sejarah',
     '/c/buku/kesehatan',
     '/c/buku/hobi-keterampilan',
     '/c/buku/politik',
     '/c/buku/ekonomi-akuntansi',
     '/c/buku/hukum',
     '/c/buku/sastra',
     '/c/buku/majalah',
     '/c/buku/bahasa',
     '/c/buku/biografi',
     '/c/buku/teknik',
     '/c/buku/kedokteran',
     '/c/buku/manajemen',
     '/c/buku/psikologi',
     '/c/buku/desain',
     '/c/buku/masak',
     '/c/buku/pertanian',
     '/c/buku/parenting',
     '/c/buku/seni-budaya',
     '/c/buku/kamus',
     '/c/buku/pariwisata',
     '/c/buku/fashion-design',
     '/c/buku/filsafat',
     '/c/buku/kesekretariatan',
     '/c/fashion',
     '/c/fashion/pria',
     '/c/fashion/wanita',
     '/c/fashion/busana-muslim',
     '/c/fashion/couple',
     '/c/fashion/anak-perempuan',
     '/c/fashion/anak-unisex',
     '/c/fashion/anak-laki-laki',
     '/c/fashion/bahan-kain',
     '/c/rumah-tangga',
     '/c/rumah-tangga/dapur',
     '/c/rumah-tangga/home-stuff',
     '/c/rumah-tangga/kamar-tidur',
     '/c/rumah-tangga/elektronik-1111',
     '/c/rumah-tangga/furniture-interior',
     '/c/rumah-tangga/perkakas',
     '/c/rumah-tangga/tempat-penyimpanan-organizer',
     '/c/rumah-tangga/kamar-mandi',
     '/c/rumah-tangga/alat-tulis',
     '/c/rumah-tangga/ruang-makan',
     '/c/rumah-tangga/perlengkapan-kebersihan',
     '/c/rumah-tangga/jam',
     '/c/rumah-tangga/gembok-kunci-pengaman',
     '/c/rumah-tangga/payung',
     '/c/personal-care',
     '/c/personal-care/produk-kecantikan',
     '/c/personal-care/produk-kesehatan',
     '/c/personal-care/lain-lain-747',
     '/c/personal-care/women-s-perfume',
     '/c/personal-care/men-s-perfume',
     '/c/personal-care/men-s-grooming',
     '/c/personal-care/self-defense',
     '/c/hobi',
     '/c/hobi/mainan',
     '/c/hobi/koleksi',
     '/c/hobi/berkebun',
     '/c/hobi/alat-musik',
     '/c/hobi/video-game',
     '/c/hobi/pet-food-stuff',
     '/c/batu-cincin',
     '/c/batu-cincin/batu-akik',
     '/c/batu-cincin/batu-mulia-permata',
     '/c/batu-cincin/rough-bahan-batu-akik',
     '/c/batu-cincin/aksesoris-perkakas',
     '/c/batu-cincin/lain-lain-1312',
     '/c/motor-471',
     '/c/motor-471/sparepart-motor',
     '/c/motor-471/aksesoris-motor',
     '/c/motor-471/outwear-motor',
     '/c/motor-471/produk-perawatan-motor',
     '/c/motor-471/motor-1545',
     '/c/komputer',
     '/c/komputer/aksesoris-226',
     '/c/komputer/hardware-1510',
     '/c/komputer/printer',
     '/c/komputer/laptop',
     '/c/komputer/software-original',
     '/c/komputer/tablet',
     '/c/komputer/desktop',
     '/c/komputer/monitor',
     '/c/komputer/scanner',
     '/c/komputer/server',
     '/c/handphone',
     '/c/handphone/case-cover',
     '/c/handphone/aksesoris-handphone',
     '/c/handphone/screen-protector',
     '/c/handphone/baterai-176',
     '/c/handphone/hp-smartphone',
     '/c/handphone/kartu-perdana-183',
     '/c/handphone/power-bank',
     '/c/handphone/charger-177',
     '/c/handphone/headset-earphone',
     '/c/handphone/kabel-data',
     '/c/handphone/memory-card',
     '/c/handphone/tongsis',
     '/c/handphone/lensa-portabel',
     '/c/handphone/mini-tripod',
     '/c/handphone/tomsis',
     '/c/onderdil-mobil',
     '/c/onderdil-mobil/aksesoris-mobil',
     '/c/onderdil-mobil/eksterior-mobil',
     '/c/onderdil-mobil/mesin-mobil',
     '/c/onderdil-mobil/kaki-kaki-mobil',
     '/c/onderdil-mobil/audio-mobil',
     '/c/onderdil-mobil/interior-mobil',
     '/c/onderdil-mobil/produk-perawatan-mobil',
     '/c/onderdil-mobil/exhaust-system-mobil',
     '/c/olahraga',
     '/c/olahraga/jersey',
     '/c/olahraga/sepakbola-futsal',
     '/c/olahraga/outdoor',
     '/c/olahraga/exercise-fitness',
     '/c/olahraga/adventure',
     '/c/olahraga/lain-lain-329',
     '/c/olahraga/lari',
     '/c/olahraga/pancing',
     '/c/olahraga/badminton',
     '/c/olahraga/renang',
     '/c/olahraga/hunting',
     '/c/olahraga/basket',
     '/c/olahraga/tenis-meja',
     '/c/olahraga/tenis',
     '/c/olahraga/roller-skate',
     '/c/olahraga/golf',
     '/c/olahraga/skateboard',
     '/c/perlengkapan-bayi',
     '/c/perlengkapan-bayi/pakaian-bayi',
     '/c/perlengkapan-bayi/mainan-663',
     '/c/perlengkapan-bayi/lain-lain-741',
     '/c/perlengkapan-bayi/perlengkapan-makan',
     '/c/perlengkapan-bayi/feeding-nursing',
     '/c/perlengkapan-bayi/perlengkapan-mandi',
     '/c/perlengkapan-bayi/perlengkapan-tidur',
     '/c/perlengkapan-bayi/diapering',
     '/c/perlengkapan-bayi/makanan-711',
     '/c/perlengkapan-bayi/perawatan-tubuh',
     '/c/perlengkapan-bayi/stroller-walker',
     '/c/perlengkapan-bayi/baby-carrier',
     '/c/perlengkapan-bayi/bouncer-chair',
     '/c/perlengkapan-bayi/furniture',
     '/c/kamera',
     '/c/kamera/baterai',
     '/c/kamera/aksesoris-kamera',
     '/c/kamera/tas-case',
     '/c/kamera/filter',
     '/c/kamera/charger',
     '/c/kamera/monopod-tripod-rig',
     '/c/kamera/hood-cap',
     '/c/kamera/kamera-digital',
     '/c/kamera/cctv',
     '/c/kamera/lensa',
     '/c/kamera/spy-cam',
     '/c/kamera/perlengkapan-studio',
     '/c/kamera/memory-card-194',
     '/c/kamera/converter-adapter',
     '/c/kamera/flash',
     '/c/kamera/strap',
     '/c/kamera/remote-trigger',
     '/c/kamera/sparepart-kamera',
     '/c/kamera/handycam',
     '/c/kamera/kamera-analog',
     '/c/kamera/action-camera',
     '/c/elektronik',
     '/c/elektronik/lain-lain-208',
     '/c/elektronik/speaker',
     '/c/elektronik/headphone',
     '/c/elektronik/baterai-681',
     '/c/elektronik/komponen-elektronik',
     '/c/elektronik/portable-audio-player',
     '/c/elektronik/aksesoris-tv-video',
     '/c/elektronik/gps',
     '/c/elektronik/radio-komunikasi',
     '/c/elektronik/proyektor',
     '/c/elektronik/televisi',
     '/c/elektronik/cd-vcd-dvd-player',
     '/c/elektronik/media-player-set-top-box',
     '/c/elektronik/home-theater',
     '/c/elektronik/radio-tape',
     '/c/sepeda',
     '/c/sepeda/outwear',
     '/c/sepeda/equipment-tools',
     '/c/sepeda/drivetrain',
     '/c/sepeda/steering',
     '/c/sepeda/wheel-hub-rims-dll',
     '/c/sepeda/fullbike',
     '/c/sepeda/saddle-seatpost',
     '/c/sepeda/brake',
     '/c/sepeda/frame',
     '/c/sepeda/fork-suspension',
     '/c/sepeda/part-generik',
     '/c/sepeda/nutrition-energy',
     '/c/food',
     '/c/food/cemilan-snack',
     '/c/food/makanan',
     '/c/food/minuman',
     '/c/food/bahan-mentah',
     '/c/food/bumb',
     '/c/food/dairy-products',
     '/c/perlengkapan-kantor']

  shuffle(categories)
  start_urls = ["https://www.bukalapak.com" + c for c in categories]

  rules=(
      Rule(
            LinkExtractor(allow = "https://www.bukalapak.com/p/.*",
                          unique = True,
                          process_value=lambda l: l.split('?')[0]), 
            callback = "parse_product", 
            follow=True
        ),
      )

  def parse_product(self, response):
    self.logger.debug("parse_product")
    try:
      shopUrlXPaths = ["//article[@class='user-display']//h5[@class='user__name']/a/@href"]
      shop_url = Util.GetValueByXpaths(self.logger, response, shopUrlXPaths)
      shop_url = response.urljoin(shop_url)
      self.logger.debug("shop_url: %s" % shop_url)
    except Exception as e:
      import sys
      self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      self.logger.error('str(e) %s' %  str(e))
      self.logger.error(e.message)
      raise e
    else:
      yield EcMarketAnalyzer.ToRecordProductsInShop(self.logger, shop_url)
