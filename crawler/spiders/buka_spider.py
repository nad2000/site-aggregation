# -*- coding: utf-8 -*-

from scrapy import Spider
from crawler.price_comparison.price_comparison import PriceComparison
from scrapy import Request
from crawler.bukalapak.ec_market_analyzer import EcMarketAnalyzer
from crawler.itemloader import DefaultItemLoader
from crawler.items import ShopItem 
from crawler.util import Util

'''
To crawl my shops, use 
  scrapy crawl myShopSpider

To crawl all shops, use
  scrapy crawl allShopSpider
'''

class BukaSpider(Spider):
  name = 'bukaSpider'
  allowed_domains = ['bukalapak.com']

  def __init__(self, service="", *args, **kwargs):
    super(BukaSpider, self).__init__(*args, **kwargs)
    self.service = service

  def start_requests(self):
    self.logger.debug("self.service: %s" % self.service)

    if self.service == 'CollectShopsFromKeywords':
      from crawler.private_settings import seedKeywords
      from crawler.bukalapak.shop_collector import ShopCollector
      self.logger.debug("CollectShopsFromKeywords")
      for x in ShopCollector.CollectShopsFromKeywords(self.logger, seedKeywords):
        yield x

    elif self.service == 'CrawlMyShop':
      for request in EcMarketAnalyzer.RecordProductsInShops(self.logger, self.GetSeedShopUrlsFromSettings()):
        yield request

    elif self.service == 'CrawlShopsInDb':
      for request in EcMarketAnalyzer.RecordProductsInShops(self.logger, self.GetShopUrlsFromDb()):
        yield request

    elif self.service == 'UpdateProductMetrics':
      for request in self.UpdateProductMetrics():
        yield request

    elif self.service == 'UpdateStatusOfShopsInDb':
      self.logger.debug('UpdateStatusOfShopsInDb')
      from crawler.db import Shop
      EcMarketAnalyzer.logger = self.logger
      for shop in Shop.select().distinct():
        try:
          self.logger.debug("shop.url: %s" % shop.url)
          request = Request( shop.url, callback = self.UpdateStatusOfShopsInDb)
        except Exception as e:
          import sys
          self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
          self.logger.error('str(e) %s' %  str(e))
          self.logger.error(e.message)
          raise e
        yield request

    elif self.service == 'AnalyzeKeyword':
      from crawler.keyword_crawler.keyword_crawler import KeywordCrawler
      keywordCrawler = KeywordCrawler()
      for request in keywordCrawler.CrawlKeywords():
        yield request

      # It takes time for parse_xxx to issue Items into pipeline (the html code takes time to return).
      # The crawler stop immediately if pipeline does not have Items. 
      # Need to wait for a while manually to prevent the crawler stopping.
      import time
      time.sleep(5)

    elif self.service == 'PriceComparison':
      # Dont know why but native python logger cannot be used in price_comparison.py.
      # I need to borrow scrapy's logger.
      for request in PriceComparison.ComparePrice(self.logger):
        yield request

  def GetSeedShopUrlsFromSettings(self):
    shopUrls = []
    from crawler.settings import ShopUrls as shopUrls
    assert len(shopUrls)
    return shopUrls

  def GetShopUrlsFromDb(self):
    from crawler.db import Shop
    shopUrls = [shop.url for shop in Shop.select(Shop.url)]
    assert len(shopUrls)
    return shopUrls

  def UpdateProductMetrics(self):
    from crawler.db import Product, Metrics
    EcMarketAnalyzer.logger = self.logger
    for product in Product.select().join(Metrics).where(Metrics.price>140000, Metrics.sales>30).distinct():
      self.logger.debug("product.url: %s" % product.url)
      request = Request(
        product.url,
        callback = EcMarketAnalyzer.parse_Product,
        meta={
          'data': {
            'shopReference': None 
          },
        },
      )
      yield request

  def UpdateStatusOfShopsInDb(self, response):
    try:
      self.logger.debug("UpdateStatusOfShopsInDb")
      shopItem = DefaultItemLoader(ShopItem(), response=response)

      shopItem.add_value('url', response.url)

      # feedback is something like u'94% (95 feedback)'
      feedback = Util.GetValueByXpaths(self.logger, response, ["//a[@class='user-feedback-summary']/text()"])

      import re
      m = re.search('([0-9]+)% \(([0-9]+) feedback\)', feedback)
      assert m, 'abnormal feedback format'
      feedbackNumber = int(m.group(2)) 
      score = float(m.group(1)) / 100

      shopItem.add_value('feedbackNumber', feedbackNumber)
      shopItem.add_value('score', score)

      shopItem = shopItem.load_item()
    except Exception as e:
      import sys
      self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      self.logger.error('str(e) %s' %  str(e))
      self.logger.error(e.message)
    else:
      yield shopItem

