from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from crawler.itemloader import DefaultItemLoader
from crawler.util import Util
from crawler.items import ProductItem
from crawler.lazada.util import PriceStringToNumber

xpaths = {
        'price': ['//span[@id="price_box"]/text()'],
        'title': ['//*[@id="prod_title"]/text()'],
        'commentNumber': ['//*[@id="reviewslist"]/div[3]/h4'],
        'score': ['//div[@class="ratingStarsResult"]//span[@class="ratingNumber"]/text()'],
        }

class LazaAllShopSpider(CrawlSpider):
  name = 'lazadaAllShopSpider'
  allowed_domains = ['lazada.co.id']
  start_urls = [ 'http://www.lazada.co.id', ]
  rules=(
      Rule(
        LinkExtractor(allow = "http://www.lazada.co.id/.*"), 
        callback = "parse_product", 
        follow=True
        ),
      )

  # def __init__(self):
    # self.logger.debug("LazaAllShopSpider")
    # self.xpaths = {
            # 'price': ['//span[@id="price_box"]/text()'],
            # 'commentNumber': ['//*[@id="reviewslist"]/div[3]/h4'],
            # 'score': ['//div[@class="ratingStarsResult"]//span[@class="ratingNumber"]/text()'],
            # }
  def parse_product(self, response):
    self.logger.debug("parse_product")
    try:
      self.logger.debug("parse_product"  + (", \n response.url: %s" %  response.url))
      productReference = self.UpdateProductItem(response) 
      metricsItem = self.LoadMetricsItem(response, productReference)
    except StopIteration:
      return 
    except Exception as e:
      import sys
      self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      self.logger.error('str(e) %s' %  str(e))
      self.logger.error(e.message)
    else:
      self.logger.debug("yield str(metricsItem): %s" % str(metricsItem))
      yield metricsItem

  def UpdateProductItem(self, response):
    '''
    Do not merge this method with EcMarketAnalyzer.__UpdateProduct. 
    We cannot reuse EcMarketAnalyzer.__GetMetricsItemForUpdate because __GetMetricsItemForUpdate makes an extra xhr call to get metrics. In laza, we can get metrics without calling xhr.
    EcMarketAnalyzer.__UpdateProduct is not long. Is not so meaningful to reuse it.
    '''
    try:
      from crawler.db import Product
      from crawler.settings import DEBUG_MODE

      product = Product()
      products = Product.select().where(Product.url == response.url) 
      if products.count() > 0: 
        product = products[0]
      product.url = response.url

      title = Util.GetValueByXpaths(self.logger, response, xpaths['title'])
      # title here would be like "\n                Regency Deluxe Mini Tornado Fan DLX 6 Inch Silver Hitam                \xc2\xa0\n". 
      title = ' '.join(title.split())
      product.title = title
      if not DEBUG_MODE:
        product.save()
    except Exception as e:
      import sys
      self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      self.logger.error('str(e) %s' %  str(e))
      self.logger.error(e.message)
    return product

  def LoadMetricsItem(self, response, productReference):
    try:
      from crawler.items import MetricsItem

      metricsItem = DefaultItemLoader(MetricsItem(), response=response)

      price = Util.GetValueByXpaths(self.logger, response, xpaths['price'])
      if price == '-1': 
        return None
      price = PriceStringToNumber(self.logger, price)
      if price == -1: 
        return None
      metricsItem.add_value('price', str(price))

      commentNumber = Util.GetValueByXpaths(self.logger, response, xpaths['commentNumber'])
      if commentNumber == '-1': 
        return None

      import re
      m = re.search('Ulasan Pelanggan \(Total ([0-9]+)\)', commentNumber)
      if not m: 
        self.logger.warning("something is wrong in comment number format."  
            + (", \n commentNumber: %s" %  commentNumber)  
            + (", \n response.url: %s" %  response.url))
        return None
      commentNumber = int(m.group(1))
      metricsItem.add_value('commentNumber', commentNumber)

      metricsItem.add_value('product', productReference)

      metricsItem = metricsItem.load_item()
      return metricsItem
    except Exception as e:
      import sys
      self.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      self.logger.error('str(e) %s' %  str(e))
      self.logger.error(e.message)

