# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from crawler.bukalapak.ec_market_analyzer import EcMarketAnalyzer
from crawler.util import Util
from crawler.product_db import Product
from crawler.items import ProductItem
from random import shuffle


# 1st and 2nd level categories
categories = [
#    'buk', 'buku/novel', 'buku/agama-kepercayaan', 'buku/komik',
    'buku/anak-anak', 'buku/pendidikan', 'buku/lain-lain-506',
#    'buku/komputer-487', 'buku/pengembangan-diri', 'buku/sosial',
#    'buku/bisnis', 'buku/sejarah', 'buku/kesehatan',
#    'buku/hobi-keterampilan', 'buku/politik', 'buku/ekonomi-akuntansi',
#    'buku/hukum', 'buku/sastra', 'buku/majalah', 'buku/bahasa',
#    'buku/biografi', 'buku/teknik', 'buku/kedokteran',
#    'buku/manajemen', 'buku/psikologi', 'buku/desain', 'buku/masak',
#    'buku/pertanian', 'buku/parenting', 'buku/seni-budaya',
#    'buku/kamus', 'buku/pariwisata', 'buku/fashion-design',
#    'buku/filsafat', 'buku/kesekretariatan', 'fashion', 'fashion/pria',
#    'fashion/wanita', 'fashion/busana-muslim', 'fashion/couple',
#    'fashion/anak-perempuan', 'fashion/anak-unisex',
#    'fashion/anak-laki-laki', 'fashion/bahan-kain', 'rumah-tangga',
#    'rumah-tangga/dapur', 'rumah-tangga/home-stuff',
#    'rumah-tangga/kamar-tidur', 'rumah-tangga/elektronik-1111',
#    'rumah-tangga/furniture-interior', 'rumah-tangga/perkakas',
#    'rumah-tangga/tempat-penyimpanan-organizer',
#    'rumah-tangga/kamar-mandi', 'rumah-tangga/alat-tulis',
#    'rumah-tangga/ruang-makan', 'rumah-tangga/perlengkapan-kebersihan',
#    'rumah-tangga/jam', 'rumah-tangga/gembok-kunci-pengaman',
#    'rumah-tangga/payung', 'personal-care',
#    'personal-care/produk-kecantikan',
#    'personal-care/produk-kesehatan', 'personal-care/lain-lain-747',
#    'personal-care/women-s-perfume', 'personal-care/men-s-perfume',
#    'personal-care/men-s-grooming', 'personal-care/self-defense',
#    'hobi', 'hobi/mainan', 'hobi/koleksi', 'hobi/berkebun',
#    'hobi/alat-musik', 'hobi/video-game', 'hobi/pet-food-stuff',
#    'batu-cincin', 'batu-cincin/batu-akik',
#    'batu-cincin/batu-mulia-permata',
#    'batu-cincin/rough-bahan-batu-akik',
#    'batu-cincin/aksesoris-perkakas', 'batu-cincin/lain-lain-1312',
#    'motor-471', 'motor-471/sparepart-motor',
#    'motor-471/aksesoris-motor', 'motor-471/outwear-motor',
#    'motor-471/produk-perawatan-motor', 'motor-471/motor-1545',
#    'komputer', 'komputer/aksesoris-226', 'komputer/hardware-1510',
#    'komputer/printer', 'komputer/laptop',
#    'komputer/software-original', 'komputer/tablet',
#    'komputer/desktop', 'komputer/monitor', 'komputer/scanner',
#    'komputer/server', 'handphone', 'handphone/case-cover',
#    'handphone/aksesoris-handphone', 'handphone/screen-protector',
#    'handphone/baterai-176', 'handphone/hp-smartphone',
#    'handphone/kartu-perdana-183', 'handphone/power-bank',
#    'handphone/charger-177', 'handphone/headset-earphone',
#    'handphone/kabel-data', 'handphone/memory-card',
#    'handphone/tongsis', 'handphone/lensa-portabel',
#    'handphone/mini-tripod', 'handphone/tomsis', 'onderdil-mobil',
#    'onderdil-mobil/aksesoris-mobil', 'onderdil-mobil/eksterior-mobil',
#    'onderdil-mobil/mesin-mobil', 'onderdil-mobil/kaki-kaki-mobil',
#    'onderdil-mobil/audio-mobil', 'onderdil-mobil/interior-mobil',
#    'onderdil-mobil/produk-perawatan-mobil',
#    'onderdil-mobil/exhaust-system-mobil', 'olahraga',
#    'olahraga/jersey', 'olahraga/sepakbola-futsal', 'olahraga/outdoor',
#    'olahraga/exercise-fitness', 'olahraga/adventure',
#    'olahraga/lain-lain-329', 'olahraga/lari', 'olahraga/pancing',
#    'olahraga/badminton', 'olahraga/renang', 'olahraga/hunting',
#    'olahraga/basket', 'olahraga/tenis-meja', 'olahraga/tenis',
#    'olahraga/roller-skate', 'olahraga/golf', 'olahraga/skateboard',
#    'perlengkapan-bayi', 'perlengkapan-bayi/pakaian-bayi',
#    'perlengkapan-bayi/mainan-663', 'perlengkapan-bayi/lain-lain-741',
#    'perlengkapan-bayi/perlengkapan-makan',
#    'perlengkapan-bayi/feeding-nursing',
#    'perlengkapan-bayi/perlengkapan-mandi',
#    'perlengkapan-bayi/perlengkapan-tidur',
#    'perlengkapan-bayi/diapering', 'perlengkapan-bayi/makanan-711',
#    'perlengkapan-bayi/perawatan-tubuh',
#    'perlengkapan-bayi/stroller-walker',
#    'perlengkapan-bayi/baby-carrier',
#    'perlengkapan-bayi/bouncer-chair', 'perlengkapan-bayi/furniture',
#    'kamera', 'kamera/baterai', 'kamera/aksesoris-kamera',
#    'kamera/tas-case', 'kamera/filter', 'kamera/charger',
#    'kamera/monopod-tripod-rig', 'kamera/hood-cap',
#    'kamera/kamera-digital', 'kamera/cctv', 'kamera/lensa',
#    'kamera/spy-cam', 'kamera/perlengkapan-studio',
#    'kamera/memory-card-194', 'kamera/converter-adapter',
#    'kamera/flash', 'kamera/strap', 'kamera/remote-trigger',
#    'kamera/sparepart-kamera', 'kamera/handycam',
#    'kamera/kamera-analog', 'kamera/action-camera', 'elektronik',
#    'elektronik/lain-lain-208', 'elektronik/speaker',
#    'elektronik/headphone', 'elektronik/baterai-681',
#    'elektronik/komponen-elektronik',
#    'elektronik/portable-audio-player',
#    'elektronik/aksesoris-tv-video', 'elektronik/gps',
#    'elektronik/radio-komunikasi', 'elektronik/proyektor',
#    'elektronik/televisi', 'elektronik/cd-vcd-dvd-player',
#    'elektronik/media-player-set-top-box', 'elektronik/home-theater',
#    'elektronik/radio-tape', 'sepeda', 'sepeda/outwear',
#    'sepeda/equipment-tools', 'sepeda/drivetrain', 'sepeda/steering',
#    'sepeda/wheel-hub-rims-dll', 'sepeda/fullbike',
#    'sepeda/saddle-seatpost', 'sepeda/brake', 'sepeda/frame',
#    'sepeda/fork-suspension', 'sepeda/part-generik',
#    'sepeda/nutrition-energy', 'food', 'food/cemilan-snack',
#    'food/makanan', 'food/minuman', 'food/bahan-mentah', 'food/bumb',
#    'food/dairy-products', 'perlengkapan-kantor',
]
shuffle(categories)


def product_urls_filter(links):
    """
    Filter out links that have been processed already (they are in DB)
    """
    urls_in_db = [r[0] for r in Product.select(Product.id).where(
        Product.url << [l.url for l in links]).distinct().tuples()]
    return [l for l in links if l.url not in urls_in_db]


class ProductsSpider(CrawlSpider):
    name = "products"
    allowed_domains = ["www.bukalapak.com"]
    start_urls = ["https://www.bukalapak.com/c/" + c for c in categories]

    rules = (
        Rule(
            LinkExtractor(
                allow="https://www.bukalapak.com/p/.*",
                unique=True,
                process_value=lambda l: l.split('?')[0]),
            callback="parse_product",
            follow=True,
            #process_links=product_urls_filter
        ),
    )

    def parse_product(self, response):
        self.logger.debug("parse_product")
        try:
            shopUrlXPaths = [
                "//article[@class='user-display']//h5[@class='user__name']/a/@href"]
            shop_url = Util.GetValueByXpaths(
                self.logger, response, shopUrlXPaths)
            shop_url = response.urljoin(shop_url)
            self.logger.debug("shop_url: %s" % shop_url)
            
            ## Extract product title, categories, and descitiption
            #price_amount = response.xpath("//span[@itemprop='price']/span[@class='amount positive']/text()")
            title = response.xpath('//h1[@class="product-detailed__name" and @itemprop="name"]/text()').extract_first().strip()

            description = ""
            for ss in response.xpath('//div[@id and div[@class]]'):
                if ss.select("@id").extract_first().strip().startswith(u"product_desc_"):
                    description = '\n'.join(ss.xpath("div[@class]/p/text()").extract())
                    if description:
                        description = description.strip()
                    break
                
            category = response.xpath('//dd[@class="kvp__value" and @itemprop="category"]/text()').extract_first()
            if category:
                category = category.strip()

            yield ProductItem(
                url = response.url,
                title = title,
                category = category,
                description = description)
                
        except Exception as e:
            import sys
            self.logger.error('Error on line {}'.format(
                sys.exc_info()[-1].tb_lineno))
            self.logger.error('str(e) %s' % str(e))
            self.logger.error(e.message)
            raise e
            