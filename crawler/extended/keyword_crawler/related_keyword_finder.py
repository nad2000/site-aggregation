from selenium.webdriver.common.keys import Keys
import time
from scrapy.http import FormRequest
from db import mysql_db, Keywords
from selenium.common.exceptions import TimeoutException
from google_ad_api import GoogleAdApi
import sys

def ImportSeleniumLib():
  import socket
  hostName = socket.gethostname()

  libPaths = {
      'XLING-LAB': r'y:\base\documents\scripts\programming\crossplatform\auto_test\selenium',
      'XLING-LAB2': r'y:\base\documents\scripts\programming\crossplatform\auto_test\selenium',
      'leonexu121106': r'z:\home\xling\base\documents\scripts\programming\crossplatform\auto_test\selenium',
      'xling-VirtualBox': r'/home/xling/base/documents/scripts/programming/crossplatform/auto_test/selenium'
      }
  libPath = libPaths[hostName]
  print ("libPath: %s" % libPath)

  import sys, importlib
  sys.path.insert(0, libPath)
  return importlib.import_module('lib.base', None)

def CountDown(seconds, comment=''):
  seconds = int(seconds)
  for i in range(seconds):
    print comment, i
    time.sleep(1)
  print

class RelatedKeywordFinder():
  seleniumLib = ImportSeleniumLib()
  browser = seleniumLib.InitSelenium()

  @classmethod
  def GatherPlaceholderKeywordsFromSeedsAndWriteIntoDb(cls, seedKeywords):
    '''
    Accept a set of seedKeywords.
      Each seedKeyword can generate two set of related keywords using google ad api and ubersuggest.org.
      The generated keywords are saved into DB.
    Scan each seedKeyword. If their related keywords have not yet existed in DB, generate their related keywords and save into DB. 
    '''
    for seedKeyword in seedKeywords:
      keywords = cls.GetKeywordsFromGoogle(seedKeyword)
      cls.SavePlaceholderKeywords(seedKeyword, keywords, 'google-adwords')

      keywords = cls.GetKeywordsFromUbersuggest(seedKeyword)
      cls.SavePlaceholderKeywords(seedKeyword, keywords, 'ubersuggest')

  @classmethod
  def GetKeywordsFromGoogle(cls, seedKeyword):
    if(Keywords.filter(seedKeyword = seedKeyword, seedExpander = 'google-adwords').count() > 0):
      return []

    return GoogleAdApi.GenerateRelatedKeywords(seedKeyword)

  @classmethod
  def GetKeywordsFromUbersuggest(cls, seedKeyword):
    if(Keywords.filter(seedKeyword = seedKeyword, seedExpander = 'ubersuggest').count() > 0):
      return []

    return cls.GetRelatedKeywordsFromUbersuggest(seedKeyword)

  @classmethod
  def GetRelatedKeywordsFromUbersuggest(cls, seedKeyword):
    '''
    Cannot search keywords by passing a url.
    I have analyzed xhr of ubersuggest.com but I cannot recognize which js generated the dynamic keywords.
    So, I try selenium.
    '''
    assert cls.browser
    cls.FindElementWithTimeoutRetry(cls.browser.get, 'http://ubersuggest.org/')
    cls.FindElementWithTimeoutRetry(cls.browser.find_element_by_css_selector, '#query').send_keys(seedKeyword) # query
    cls.FindElementWithTimeoutRetry(cls.browser.find_element_by_css_selector, '#language > option:nth-child(23)').click() # laungage
    cls.FindElementWithTimeoutRetry(cls.browser.find_element_by_css_selector, '#uberform > input[type="submit"]:nth-child(7)').click() # submit
    CountDown(5, 'wait keywords to show')

    keywords = cls.FindElementWithTimeoutRetry(cls.browser.find_elements_by_xpath, '/html/body/div[2]/div[3]/div[1]//span')

    # ubersuggest is strict to crawlers. 
    # Be careful to avoid being banned.
    keywords = [keyword.text for keyword in keywords]
    print ("seedKeyword: %s" % seedKeyword)
    print ("Ubersuggest keywords: %s" % keywords)
    CountDown(20, 'avoid being banned')

    if len(keywords) == 0:
      '''
      I use X = (Keywords.filter(seedKeyword = seedKeyword, seedExpander = 'ubersuggest').count() > 0)
      to judge whether crawl ubersuggest for a seedKeyword. 
      By return a dummy keyword, we can make X be true and avoid
      crawling ubersuggest for this seedKeyword any more.
      '''
      return ['%s@%s' % (seedKeyword, 'ubersuggest')]
    return keywords

  @classmethod
  def FindElementWithTimeoutRetry(cls, func, arguments):
    timeToWaitBeforeNextTry = 15
    while(1):
      try:
        return func(arguments)
      except TimeoutException:
        CountDown(timeToWaitBeforeNextTry, 'timeouted, try again')
        timeToWaitBeforeNextTry *= 2
        if timeToWaitBeforeNextTry > 2000:
          print 'give up, arguments: %s' % str(arguments)
          #cls.browser.close()
          sys.exit(1)
      except Exception as e:
        print e.__doc__
        print e.message
        print ("arguments: %s" % arguments)
        #cls.browser.close()
        sys.exit(1)

  @classmethod
  def SavePlaceholderKeywords(cls, seedKeyword, keywords, seedExpander):
    for keyword in keywords:
      keywordEntry = Keywords()
      keywordEntry.keyword = keyword
      keywordEntry.seedKeyword = seedKeyword
      keywordEntry.seedExpander = seedExpander

      keywordEntry.averageCpc = -1
      keywordEntry.competition = -1
      keywordEntry.productNumber = -1
      keywordEntry.searchVolume = -1

      print 'keyword:%s, save: seedKeyword:%s, seedExpander:%s, ' % (keyword, seedKeyword, seedExpander)
      try:
        print ("keywordEntry.save(): %s" % keywordEntry.save())
      except: 
        # possible error: duplicated key.
        pass

if __name__ == '__main__':
  seedKeywords = []
  from crawler.private_settings import seedKeywords
  assert len(seedKeywords)
  RelatedKeywordFinder.GatherPlaceholderKeywordsFromSeedsAndWriteIntoDb(seedKeywords)
