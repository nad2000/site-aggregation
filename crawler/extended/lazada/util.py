def PriceStringToNumber(logger, priceStr):
  '''
  This method is used by multiple classes. 
  '''
  if priceStr[0:3] != "RP ": 
    logger.error('priceStr is not heading with RP.'  + (", \n priceStr: %s" %  priceStr))
    return -1
  priceStr = priceStr[3:].replace(".", "")
  if priceStr[-1] == ',':
    priceStr = priceStr[:-1]
  
  price = int(priceStr)
  if price < 0: 
    logger.error('price is negative! %s' % price)
    return -1
  return price
