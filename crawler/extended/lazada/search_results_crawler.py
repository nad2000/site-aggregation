from crawler.lazada.util import PriceStringToNumber

class SearchResultsCrawler():
  '''
  analyze the search results page of keywords on lazada, such as 'http://www.lazada.co.id/catalog/?q=games+memasak'
  '''
  urlPaths = [
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/@href",
      "/html/body/div[2]/div[2]/div[2]/div[1]/div[3]/a[1]/@href", #http://www.lazada.co.id/beli-handphone/samsung/
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/div[1]/@data-original", #http://www.lazada.co.id/catalog/?q=games+memasak
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/div[1]/a/@href", #http://www.lazada.co.id/catalog/?q=games+memasak
      ]
  #lazadaProductTitle = "/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/div[2]/img/@title"
  titleXpaths = [
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/div[3]/div[1]/span/@title",
      "/html/body/div[2]/div[2]/div[2]/div[2]/div[2]/a[1]/div[1]/span[1]/img[1]/@title",# http://www.lazada.co.id/fashion-wanita/
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/div[1]/a/div[3]/div[1]/span/@title",#http://www.lazada.co.id/catalog/?q=buku+mimpi
      ]
  # the xpath for price has many variations
  priceXpaths = [ 
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/div[3]/div[2]/div[1]/div/text()", #http://www.lazada.co.id/catalog/?q=gemscool, seems to be discount price
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/div[3]/div[2]/div[2]/text()", #http://www.lazada.co.id/catalog/?q=buku+mimpi, normal price (?)
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/div[3]/div[2]/div[3]/div/text()", # http://www.lazada.co.id/catalog/?q=buku+mimpi, sale price(?)]
      "/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/div[3]/div[2]/div[2]/text()", # cannot remember what url this 
      ]
  productImagePaths = [
      '/html/body/div[2]/div[2]/div[2]/div/div[2]/a[1]/div[2]/img/@data-original',
      '/html/body/div[2]/div[2]/div[2]/div/div[2]/div[1]/a/div[2]/img/@data-original', #http://www.lazada.co.id/catalog/?q=buku+mimpi
      ]

  @classmethod
  def GetUrl(cls, response, logger = None):
    for urlXpath in cls.urlPaths:
      if len(response.xpath(urlXpath)) == 0:
        continue
      return response.xpath(urlXpath)[0].extract()
    else:
      return ""

  @classmethod
  def GetTitle(cls, response, logger = None):
    for titleXpath in cls.titleXpaths:
      if len(response.xpath(titleXpath)) == 0:
        continue
      return response.xpath(titleXpath)[0].extract()
    else:
      return ""

  @classmethod
  def GetImagePath(cls, response, logger = None):
    try:
      for productImagePath in cls.productImagePaths:
        if not len(response.xpath(productImagePath)):
          continue
        imagePath = response.xpath(productImagePath)[0].extract()

        '''
        imagePath is the url of a small icon image:
          http://srv-live-01.lazada.co.id/p/titik-media-pemberontakan-cinderella-1-awal-dari-sebuah-mimpi-4392-611534-1-catalog.jpg
        Need to change to url of a larger image:
          http://srv-live-01.lazada.co.id/p/titik-media-pemberontakan-cinderella-1-awal-dari-sebuah-mimpi-4392-611534-1-product.jpg
        '''
        logger.debug("imagePath: %s" % imagePath)
        assert imagePath[-11:] == 'catalog.jpg'
        imagePath = imagePath[:-11] + 'product.jpg'
        return imagePath
      else:
        logger.warning("cannot find image path. \nresponse.url: %s" % response.url  + (", \ncls.productImagePath: %s" % productImagePath))
        return ''
    except Exception as e:
      import sys
      logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      logger.error(e.message)
      raise e

  @classmethod
  def GetPrice(cls, response, logger = None):
    for priceXpath in cls.priceXpaths:
      if len(response.xpath(priceXpath)) == 0:
        continue

      price = response.xpath(priceXpath)[0].extract()
      return PriceStringToNumber(price)
    else:
      return -1
