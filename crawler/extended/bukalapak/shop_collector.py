from scrapy import Request
from crawler.items import ShopItem, ProductItem, MetricsItem
from crawler.util import Util

class ShopCollector():
  xpaths = {
    'nextShopPage': ['//a[@class="next_page"]/@href'],
    }
  @classmethod
  def CollectShopsFromKeywords(cls, logger, keywords):
    '''
    Collect shops from keywords and save the shop info into DB.
    '''
    cls.logger = logger
    cls.logger.debug("CollectShopsFromKeywords")
    for keyword in keywords:
      # kewword is like '4g lte modem'
      url = 'https://www.bukalapak.com/products?utf8=%E2%9C%93&search%5Bkeywords%5D='
      url += '+'.join(keyword.split())
      cls.logger.debug("url to search: %s" % url)
      yield Request(url, callback = cls.__CrawlSellerShopsFromKeyword)

  @classmethod
  def __CrawlSellerShopsFromKeyword(cls, response):
    cls.logger.debug("__CrawlSellerShopsFromKeyword")
    # record shops on the current page
    for shopUrl in response.xpath("//h5[@class='user__name']/a/@href"):
      try:
        shopItem = ShopItem()
        shopItem['title'] = shopUrl.extract()[1:]
        shopItem['url'] = response.urljoin(shopUrl.extract())
        shopItem['feedbackNumber'] = Util.GetValueByXpaths(cls.logger, response, ["//a[@class='user-feedback-summary']/text()"])
      except Exception as e:
        import sys
        cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        cls.logger.error('str(e) %s' %  str(e))
        cls.logger.error(e.message)
        raise e
      yield shopItem

    # crawl shops on the next page
    nextPageUrl = Util.GetValueByXpaths(cls.logger, response, cls.xpaths['nextShopPage'])
    while(nextPageUrl != '-1'):
      cls.logger.debug("nextPageUrl: %s" % nextPageUrl)
      yield Request(nextPageUrl, cls.__CrawlSellerShopsFromKeyword)
