from scrapy import Request
from crawler.items import ShopItem, ProductItem, MetricsItem
from crawler.itemloader import DefaultItemLoader
from crawler.settings import DEBUG_MODE

class RelatedShopParser:
  '''
  I need to ues scrapy logger that passed to this module. 
  Declear a dummy logger here to prevent pylint from complaining.
  '''
  logger = None 
  xpath = {
    'next_page': '//div[@class="pagination"]//a[@rel="next"]/@href',
    'shop_title': '//h1[contains(@class, "page-title")]//a/text()',
    'product_url': '//li[@class="product"]//h3//a/@href',
    'product_section': '//section[@itemscope]',
    'product_title': './/h1[@itemprop="name"]/text()',
    'product_category': './/dd[@itemprop="category"]/text()',
    'product_description': './/div[contains(@id, "product_desc")]//p/text()',
    'product_price': (
      './/span[@itemprop="price"]'
      '//span[contains(@class, "amount")]/text()'
    ),
    #'product_stock': './/div[@class="product-stock"]/strong/text()',
    'product_pageviews': (
      '//div[@class="product-stats"]//dd[@title="Dilihat"]/strong/text()'
    ),
    'product_likes': (
      '//div[@class="product-stats"]//dd[@title="Peminat"]/strong/text()'
    ),
    'product_sales': (
      '//div[@class="product-stats"]//dd[@title="Terjual"]/strong/text()'
      #'/html/body/div[1]/div[3]/section[1]/div[1]/div[1]/div[6]/div[1]/dl/dd[5]/strong/text()'
    ),
  }
  xhr_url = 'https://www.bukalapak.com/products/%s/user_manage'
  xhr_headers = {'X-Requested-With': 'XMLHttpRequest'}
  crawlAllShops = False

  @classmethod
  def CrawlShops(cls):
    cls.logger.debug("CrawlShops")
    from crawler.private_settings import SHOPS
    for shop_url in SHOPS:
      cls.logger.info('Processing shop :: %s' % shop_url)
      yield cls.GenerateShopRequest(shop_url)

  @classmethod
  def GenerateShopRequest(cls, shop_url):
    url = shop_url.rstrip('/') + '/products'
    return Request(
      url,
      callback=cls.parse_shop,
      meta={
        'data': {
          'shop_url': shop_url,
          'page': 1,
        }
      },
    )

  @classmethod
  def parse_shop(cls, response):
    cls.logger.debug("parse_shop")

    try:
      if response.meta['data']['page'] == 1:
        cls._validate_url(response, 'shop')
      shop_item = response.meta['data'].get('shop_item')
    except Exception as e:
      import sys
      cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      cls.logger.error(e.message)
      raise e

    # Record currentshop if not yet
    # Due to the exist of yield, it is not easy to pack
    # it into a decent function.
    if shop_item is None:
      shop_item = DefaultItemLoader(ShopItem(), response=response)
      shop_item.add_xpath('title', cls.xpath['shop_title'])
      shop_item.add_value('url', response.meta['data']['shop_url'])
      shop_item = shop_item.load_item()
      yield shop_item
      if not DEBUG_MODE and shop_item.db_entry is None:
        return

    for request in cls.RequestDetailOfProductsOnCurrentPageOfCurrentShop(response, shop_item):
      yield request

    for request in cls.RequestNextPageOfCurrentShop(response, shop_item):
      yield request

  @classmethod
  def RequestDetailOfProductsOnCurrentPageOfCurrentShop(cls, response, shop_item):
    for product_url in response.xpath(cls.xpath['product_url']).extract():
      product_url = response.urljoin(product_url).split('?')[0] # remove query
      cls.logger.info('Processing product :: %s' % product_url)
      yield Request(
        product_url,
        callback=cls.parse_product,
        meta={
          'data': {
            'shop': shop_item['url'] if DEBUG_MODE else shop_item.db_entry
          },
        },
      )

  @classmethod
  def RequestNextPageOfCurrentShop(cls, response, shop_item):
    cls.logger.debug("RequestNextPageOfCurrentShop")
    next_page_url = response.xpath(cls.xpath['next_page']).extract_first()
    if next_page_url:
      yield Request(
        response.urljoin(next_page_url),
        callback=cls.parse_shop,
        meta={
          'data': {
            'shop_url': response.meta['data']['shop_url'],
            'shop_item': shop_item,
            'page': response.meta['data']['page'] + 1,
          }
        },
      )

  @classmethod
  def parse_product(cls, response):
    page_url = cls._validate_url(response, 'product')
    product_section = response.xpath(cls.xpath['product_section'])

    if not product_section:
      cls.logger.error(
        'No product section on the page :: %s' % page_url
      )
      return

    # record product
    product_item = DefaultItemLoader(ProductItem(), selector=product_section[0])
    product_item.add_xpath('title', cls.xpath['product_title'])
    product_item.add_xpath('category', cls.xpath['product_category'])
    product_item.add_xpath('description', cls.xpath['product_description'])
    product_item.add_value('url', page_url)
    product_item.add_value('shop', response.meta['data']['shop'])
    product_item = product_item.load_item()
    yield product_item
    if not DEBUG_MODE and product_item.db_entry is None:
      return

    # to record metric
    metrics_item = DefaultItemLoader(MetricsItem(), selector=product_section[0])
    metrics_item.add_xpath('price', cls.xpath['product_price'])
    metrics_item.add_value(
      'product',
      product_item['url'] if DEBUG_MODE else product_item.db_entry
    )
    metrics_url = cls.xhr_url % page_url.rstrip('/').split('/')[-1]
    yield Request(
      metrics_url,
      callback = cls.parse_metrics,
      headers = cls.xhr_headers,
      meta={
        'data': {'metrics_item': metrics_item},
      },
    )

    if cls.crawlAllShops:
      for request in cls.ParseShopsOfCurrentProduct(response):
        yield request

  @classmethod
  def parse_metrics(cls, response):
    metrics_item = response.meta['data']['metrics_item']
    likes = response.xpath(cls.xpath['product_likes']).extract()
    pageviews = response.xpath(cls.xpath['product_pageviews']).extract()
    metrics_item.add_value('likes', likes)
    metrics_item.add_value('pageviews', pageviews)
    metrics_item.add_value('sales', response.xpath(cls.xpath['product_sales']).extract())
    yield metrics_item.load_item()

  @classmethod
  def _validate_url(cls, response, obj=''):
    # check if the real URL of loaded page differs from the requested one
    cls.logger.debug("_validate_url")
    
    request_url = response.request.url.split('?')[0]
    response_url = response.url.split('?')[0]
    if request_url != response_url:
      cls.logger.warning(
        '%s URL mismatch :: %s != %s' %
        (obj.title(), request_url, response_url)
      )
    return response_url

  @classmethod
  def ParseShopsOfCurrentProduct(cls, response):
    '''
    response.url points to a product page.
    '''
    data_id = response.xpath('//*[@id="scarab_product_div"]/@data-item')[0].extract()
    xhrUrlForRelatedShops = 'https://www.bukalapak.com/components/products/related/%s' % data_id
    cls.logger.debug("xhrUrlForRelatedShops: %s" % xhrUrlForRelatedShops)
    yield Request(
      xhrUrlForRelatedShops,
      callback = cls.parse_related_shops,
      headers = {'X-Requested-With': 'XMLHttpRequest'}
    )

  @classmethod
  def parse_related_shops(cls, response):
    relatedShopUrls = ['https://www.bukalapak.com' + e.extract() for e in response.xpath('//h5[@class="user__name"]/a/@href')]
    cls.logger.debug("len(relatedShopUrls): %s" % len(relatedShopUrls))
    for relatedShopUrl in relatedShopUrls:
      cls.logger.debug("relatedShopUrl: %s" % relatedShopUrl)
      yield cls.GenerateShopRequest(relatedShopUrl)

