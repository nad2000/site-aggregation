# -*- coding: utf-8 -*-

'''
one line to give the program's name and a brief description
Copyright (C) 2015 copyright holder

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[](http://www.guguncube.com/1656/python-image-similarity-comparison-using-several-techniques)
'''

"""
Installation of needed libraries
sudo apt-get install -y python-pip
sudo pip install PIL numpy
"""
import os, time, re, urllib
from PIL import Image
import logging
format= '%(asctime)s - %(levelname)s - %(filename)s:%(lineno)s - %(funcName)s() - %(message)s'
format= '%(asctime)s - %(filename)s:%(lineno)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=format)
logger = logging.getLogger(__name__)
  
def TryToDownloadImage(imageRootDir, urlsOfImagesToCompare):
  filePaths = []
  for url in urlsOfImagesToCompare:
    url = url.strip()
    filePath = ImageUrl2FilePath(url, imageRootDir)
    assert filePath != ''
    mkdirPFilepath(filePath)
    if not os.path.exists(filePath):
      logger.debug("downloading image %s to %s ..."%(url, imageRootDir))
      try:
        urllib.urlretrieve(url, filePath)
      except Exception as e:
        import sys
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        logger.error(e.message)
        logger.error("url: %s" % url)
        logger.debug("filePath: %s" % filePath)
        raise e
      logger.debug("downloading done")
    filePaths.append(filePath)
  return filePaths

def ImageUrl2FilePath(imageUrl, imageRootDir):
  filename = imageUrl.split('/')[-1]
  filePath = os.path.join(imageRootDir, filename)
  return filePath
 

#def CompareSimilarity(filePaths):
  #s1 = ImageSimilarityBandsViaNumpy(filePaths[0], filePaths[1])
  #s2 = imageSimilarityHistogramViaPil(filePaths[0], filePaths[1])
  #s3 = imageSimilarityVectorsViaNumpy(filePaths[0], filePaths[1])
  #s4 = ImageSimilarityGreyscaleHashCode(filePaths[0], filePaths[1])
  #print s1, s2, s3, s4
  #return s1

#def ImageSimilarityBandsViaNumpy(filepath1, filepath2):
  #import math
  #import operator
  #import numpy

  #image1 = Image.open(filepath1)
  #image2 = Image.open(filepath2)
 
  ## create thumbnails - resize em
  #image1 = GetThumbnail(image1)
  #image2 = GetThumbnail(image2)
  
  ## this eliminated unqual images - though not so smarts....
  #if image1.size != image2.size or image1.getbands() != image2.getbands():
    #return -1
  #s = 0
  #for bandIndex, band in enumerate(image1.getbands()):
    #m1 = numpy.array([p[bandIndex] for p in image1.getdata()]).reshape(*image1.size)
    #m2 = numpy.array([p[bandIndex] for p in image2.getdata()]).reshape(*image2.size)
    #s += numpy.sum(numpy.abs(m1-m2))
  #return s
 
#def imageSimilarityHistogramViaPil(filepath1, filepath2):
  #from PIL import Image
  #import math
  #import operator
  
  #image1 = Image.open(filepath1)
  #image2 = Image.open(filepath2)
 
  #image1 = GetThumbnail(image1)
  #image2 = GetThumbnail(image2)
  
  #h1 = image1.histogram()
  #h2 = image2.histogram()
 
  #rms = math.sqrt(reduce(operator.add,  list(map(lambda a,b: (a-b)**2, h1, h2)))/len(h1) )
  #return rms
 
#def imageSimilarityVectorsViaNumpy(filepath1, filepath2):
  ## source: http://www.syntacticbayleaves.com/2008/12/03/determining-image-similarity/
  ## may throw: Value Error: matrices are not aligned . 
  #from numpy import average, linalg, dot
  #import sys
  
  #image1 = Image.open(filepath1)
  #image2 = Image.open(filepath2)
 
  #image1 = GetThumbnail(image1, stretchToFit=True)
  #image2 = GetThumbnail(image2, stretchToFit=True)
  
  #images = [image1, image2]
  #vectors = []
  #norms = []
  #for image in images:
    #vector = []
    #for pixelTuple in image.getdata():
      #vector.append(average(pixelTuple))
    #vectors.append(vector)
    #norms.append(linalg.norm(vector, 2))
  #a, b = vectors
  #aNorm, bNorm = norms
  ## ValueError: matrices are not aligned !
  #res = dot(a / aNorm, b / bNorm)
  #return res
 
#def ImageSimilarityGreyscaleHashCode(filepath1, filepath2):
  ## source: http://blog.safariflow.com/2013/11/26/image-hashing-with-python/
 
  #image1 = Image.open(filepath1)
  #image2 = Image.open(filepath2)
 
  #image1 = GetThumbnail(image1, greyscale=True)
  #image2 = GetThumbnail(image2, greyscale=True)
  
  #code1 = ImagePixelHashCode(image1)
  #code2 = ImagePixelHashCode(image2)
  ## use hamming distance to compare hashes
  #res = HammingDistance(code1,code2)
  #return res
  
#def ImagePixelHashCode(image):
  #pixels = list(image.getdata())
  #avg = sum(pixels) / len(pixels)
  #bits = "".join(map(lambda pixel: '1' if pixel < avg else '0', pixels))  # '00010100...'
  #hexadecimal = int(bits, 2).__format__('016x').upper()
  #return hexadecimal
 
#def HammingDistance(s1, s2):
  #len1, len2= len(s1),len(s2)
  #if len1!=len2: 
    #"hamming distance works only for string of the same length, so i'll chop the longest sequence"
    #if len1>len2:
      #s1=s1[:-(len1-len2)]
    #else:
      #s2=s2[:-(len2-len1)]
  #assert len(s1) == len(s2)
  #return sum([ch1 != ch2 for ch1, ch2 in zip(s1, s2)])
 
#def GetThumbnail(image, size=(128,128), stretchToFit=False, greyscale=False):
  #" get a smaller version of the image - makes comparison much faster/easier"
  #if not stretchToFit:
    #image.thumbnail(size, Image.ANTIALIAS)
  #else:
    #image = image.resize(size); # for faster computation
  #if greyscale:
    #image = image.convert("L")  # Convert it to grayscale.
  #return image
 
def mkdirPFilepath(path):
  dirpath = os.path.dirname(os.path.abspath(path))
  mkdirP(dirpath)
 
def mkdirP(path):
  import errno
  try:
    os.makedirs(path)
  except OSError as exc: # Python >2.5
    if exc.errno == errno.EEXIST and os.path.isdir(path):
      pass
    else: raise
 
def getFilename(path): 
  # cross plattform filename from a given path
  # source: http://stackoverflow.com/questions/8384737/python-extract-file-name-from-path-no-matter-what-the-os-path-format
  import ntpath
  head, tail = ntpath.split(path)
  return tail or ntpath.basename(head)
 
def main():
  imageRootDir = ''
  from crawler.private_settings import imageRootDir
  assert imageRootDir is not ''
  # each pair contain multiple urls to be compared.
  urlsOfImagesToCompare = [
      'https://s0.bukalapak.com/system4/images/5/3/6/6/2/4/0/5/large/1144-hg-00-series_(2).jpg', 
      'https://s1.bukalapak.com/system4/images/5/3/6/6/3/6/8/6/large/sd-00-qant_(1).jpg'
      ]
  filePaths = TryToDownloadImage(imageRootDir, urlsOfImagesToCompare)
  #CompareSimilarity(filePaths)
 
if __name__ == '__main__':
  main()
