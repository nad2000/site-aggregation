# -*- coding: utf-8 -*-

# MYSQL DATABASE MODELS

from peewee import *
from settings import *

# define DB
mysql_db = MySQLDatabase(MYSQL_DB, host=MYSQL_HOST, port=MYSQL_PORT,
               user=MYSQL_USER, password=MYSQL_PASSWORD)

# Connect to DB
mysql_db.connect()

# DB Exception
class DBError(Exception):
  pass

# Safe wrapper that create DB table for a given model (if doesn't yet exist)
def table(model):
  model.create_table(fail_silently=True)
  return model

# BASE MODEL
# all user defined models must be inherited from this base model
class BaseModel(Model):
  class Meta:
    database = mysql_db


def InsertOrUpdateItem(item, spider = None):
  print "process_item", item
  from crawler import db

  if not getattr(item, '_db_model', None):
    return item
  if not (
    item._db_model in db.__dict__ and
    issubclass(db.__dict__[item._db_model], db.BaseModel)
    ):
    raise db.DBError('Undefined DB model :: %s' % item._db_model)
  item.__dict__['db_entry'] = None
  try:
    model = db.__dict__[item._db_model]
    if getattr(item, '_db_search_key', None):
      if item._db_search_key not in item:
        raise Exception('Item has no value for :: _db_search_key')
      search_key_value = item[item._db_search_key]
      if not isinstance(search_key_value, basestring):
        raise Exception('Unsupported :: _db_search_key')
      query = {}
      query[item._db_search_key] = search_key_value
      try:
        db_entry = model.get(**query)
        db_entry.__dict__['_data'].update(**item)
        db_entry.save()
      except model.DoesNotExist:
        db_entry = model.create(**item)
    else:
      db_entry = model(**item)
      db_entry.save()
    item.__dict__['db_entry'] = db_entry
  except Exception as e:
    if spider == None:
      print 'DBError :: %s' %  str(e)
    else:
      spider.logger.error('DBError :: %s' %  str(e))
  else:
    return item

# ALL DB MODELS (TABLES) ARE DEFINED BELOW
#
# All supported types of fields can be found here:
# http://peewee.readthedocs.org/en/latest/peewee/models.html#field-types-table
#
# Indexes are used to achive the best perfomance of SQL queries (retrieving data)
# 'unique' indexes also protect table from duplicates (on column with such index)
# in our case URLs will always be unique
# so it has sense to use such type of indexes for this column ('url' field)
# unique indexes (at least one per table) are also used by database middleware
# to implement search-for-already-saved-entry behavior
# it lets crawler to understand if some model/item/row is already in DB
# and avoid saving duplicates on each run
#
# :param 'default': set a default value for uninitialized field
# :param 'index': create a non-unique index on a given field (DB column)
# :param 'unique': create a unique index (two entries cannot have equal values)
# :param 'related_name': specify the name for getting all related entries

# SHOP TABLE
@table
class Shop(BaseModel):
  title = CharField()
  url = CharField(unique=True)
  feedbackNumber = IntegerField()
  score = FloatField()
  lastVisited = DateTimeField(index=True)

# PRODUCT TABLE
@table
class Product(BaseModel):
  url = CharField(unique=True, index=True)
  title = CharField()
  category = CharField()
  description = CharField()
  imagePath = CharField()
  similarity = FloatField()

  # One-To-Many relationship (shop has many products)
  # sometime, shop is not important. So, set null = True
  shop = ForeignKeyField(Shop, related_name='products', null = True)
  #searchKeys = ForeignKeyField(SearchKeys, related_name = 'products' )

@table
class SearchKey(BaseModel):
  searchKey = CharField()
  subSearchKey = CharField()
  product = ForeignKeyField(Product)

# METRICS TABLE
@table
class Metrics(BaseModel):
  '''
  We trace the data of each product at different times. 
  So, each product has multiple metrics
  '''
  timestamp = DateTimeField(index=True)
  pageviews = IntegerField()
  likes = IntegerField()
  price = IntegerField()
  sales = IntegerField()
  commentNumber = IntegerField() 
  score = FloatField()
  #stock = IntegerField(default=0)
  # One-To-Many relationship (product has many metrics)
  product = ForeignKeyField(Product, related_name='metrics')

@table
class Keywords(BaseModel):
  # https://docs.djangoproject.com/en/1.8/ref/models/fields/
  # .save() always fails once I use primary_key = true. 
  # The reason is unclear.
  keyword = CharField(index=True, unique=True)
  seedKeyword = CharField()
  seedExpander = CharField()

  searchVolume = IntegerField()
  averageCpc = IntegerField()
  competition = FloatField()
  productNumber = IntegerField()

  def Init(self, _seedKeyword='', _seedExpander='', _keyword='', _searchVolume=-1, _averageCpc=-1, _competition=-1, _productNumber=-1):
    self.seedKeyword = _seedKeyword
    self.seedExpander = _seedExpander
    self.keyword = _keyword
    self.searchVolume = _searchVolume
    self.averageCpc = _averageCpc
    self.competition = _competition
    self.productNumber = _productNumber

# Close DB connection for now
# connection will be opened again later in MySQLPipeline
mysql_db.close()
