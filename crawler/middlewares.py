# -*-coding:utf-8-*-

from random import choice
from scrapy import Request
from scrapy.loader import arg_to_iter
from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware
from user_agent import generate_navigator


class RotateUserAgentMiddleware(UserAgentMiddleware):

    def __init__(self, user_agent):
        self.user_agent = user_agent

    def process_request(self, request, spider):
        user_agent = generate_navigator()['user_agent']
        request.headers.setdefault('user-agent', user_agent)
        spider.logger.debug(
            '%s User-Agent: %s' % (request, request.headers['user-agent'])
        )


class ProxyMiddleware(object):

    def __init__(self, proxies):
        if proxies:
            self.proxies = arg_to_iter(proxies)
            self._active = True
        else:
            self._active = False

    def process_request(self, request, spider):
        if self._active:
            request.meta['proxy'] = choice(self.proxies)
            spider.logger.debug(
                '%s Proxy: %s' % (request, request.meta['proxy'])
            )

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.get('HTTP_PROXIES'))


#class SpiderMiddleware(object):

#    def __init__(self, debug_mode):
#        self._active = True if debug_mode is not None else False

#    def process_spider_output(self, response, result, spider):
#        if not self._active:
#            return result

#    @classmethod
#    def from_crawler(cls, crawler):
#        return cls(crawler.settings.get('DEBUG_MODE'))

