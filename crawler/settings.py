# -*- coding: utf-8 -*-

import os

# Project name
BOT_NAME = 'crawler'

# Enable/disable debugging mode
DEBUG_MODE = False

# Project's root directory 
ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

# Logging settings
LOG_ENABLED = True
# Redirect :stdout to the log
LOG_STDOUT = False
# in DEBUG_MODE is True, then :stderr is used for logging messages
# otherwise the log file is used
if not DEBUG_MODE:
    LOG_DIR = os.path.join(ROOT_DIR,'log')
    if not os.path.exists(LOG_DIR):
        os.mkdir(LOG_DIR)
    LOG_FILE = os.path.join(LOG_DIR, 'crawler.log')
# Logging level
#LOG_LEVEL = 'DEBUG' if DEBUG_MODE else 'INFO'
LOG_LEVEL = 'DEBUG'

# Telnet console
TELNETCONSOLE_ENABLED = True
TELNETCONSOLE_PORT = [6023, 6073]

# Spiders
SPIDER_MODULES = ['crawler.spiders']
NEWSPIDER_MODULE = 'crawler.spiders'
# Rules (if zero, ignore the rule):
# Close the spider if it remains open for more than that number of seconds
CLOSESPIDER_TIMEOUT = 0
# Close the spider if it scrapes more than that amount if items
CLOSESPIDER_ITEMCOUNT = 0
# Close the spider if it crawles more than that number of pages (HTTP responses)
CLOSESPIDER_PAGECOUNT = 0
# Close the spider if it receives more than that number of errors
CLOSESPIDER_ERRORCOUNT = 0

# NETWORK SETTINGS:
# The amount of time (in secs) that the downloader will wait before timing out
DOWNLOAD_TIMEOUT = 45
# The amount of time (in secs) that the downloader should wait
# before downloading consecutive pages from the same website
# if you have download delay set to non-zero value, then crawler will work only in single thread, no matter how many concurrent requests you set above so change it to zero or just comment this line
# When it is too small, MetricsItems cannot be correctly obtained (buka will bans you).
DOWNLOAD_DELAY = 1.5 # 0
# Wait a random amount of time (between 0.5 and 1.5 * DOWNLOAD_DELAY)
# Does not matter if DOWNLOAD_DELAY is zero.
RANDOMIZE_DOWNLOAD_DELAY = True

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 16
# The maximum number of concurrent requests per domain
CONCURRENT_REQUESTS_PER_DOMAIN = 8 # 16
# The maximum number of concurrent request per IP
# If non-zero, the CONCURRENT_REQUESTS_PER_DOMAIN setting is ignored
# and download delay is enforced per IP, not per domain
CONCURRENT_REQUESTS_PER_IP = 3 # 16

# HTTP redirect
REDIRECT_ENABLED = True
# Max number of allowed redirects
REDIRECT_MAX_TIMES = 5
REDIRECT_PRIORITY_ADJUST = 2
# META redirect
METAREFRESH_ENABLED = True
METAREFRESH_MAXDELAY = 15
# Retry failed requests
RETRY_ENABLED = True
RETRY_TIMES = 2
RETRY_HTTP_CODES = [500, 502, 503, 504, 408]
# Populates 'Referer' header by using URL of the previos Response
REFERER_ENABLED = True
# AJAX
AJAXCRAWL_ENABLED = False
# The maximum depth that will be allowed to crawl for any site
# if zero, no limit will be imposed
DEPTH_LIMIT = 0
# DNS in-memory cache
DNSCACHE_ENABLED = True
DNSCACHE_SIZE = 10000
# Timeout for processing of DNS queries in seconds
DNS_TIMEOUT = 30
# Respect robots.txt policies
ROBOTSTXT_OBEY = False

# Proxy settings
PROXY_ENABLED = True # False
# List of HTTP proxies
HTTP_PROXIES = ['http://127.0.0.1:8123', ]

# HTTP request headers
DEFAULT_REQUEST_HEADERS = {
   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
   'Accept-Language': 'en',
}
# If we don't use proxies, then it has no sense to rotate User-Agent header
if not PROXY_ENABLED:
    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:22.0) Gecko/20100101 Firefox/22.0'

# Cookies
COOKIES_ENABLED = False
COOKIES_DEBUG = True if DEBUG_MODE else False

# Enable and configure the AutoThrottle extension (disabled by default)
# NOTE: AutoThrottle will honour the standard settings for concurrency and delay
#AUTOTHROTTLE_ENABLED=True
# The initial download delay
#AUTOTHROTTLE_START_DELAY=5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY=60
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG=False

# Memory settings:
# Memory debugging
MEMDEBUG_ENABLED = True
# Enable the memory usage extension that will shutdown the Scrapy process
# when it exceeds a memory limit (useful when lxml causes memory leaks)
MEMUSAGE_ENABLED = False
# The maximum amount of memory to allow (in megabytes)
# before shutting down the Scrapy
# If zero, no check will be performed
MEMUSAGE_LIMIT_MB = 0

# Maximum number of concurrent items (per response)
# to process in parallel in the Item Pipeline
CONCURRENT_ITEMS = 100

# The maximum limit for Twisted Reactor thread pool size
REACTOR_THREADPOOL_MAXSIZE = 10

# Item pipelines
ITEM_PIPELINES = {
    # default pipeline
    #'crawler.pipelines.DefaultItemPipeline': 100,
    # DB pipeline that saves items into MySQL database
    # * disabled in DEBUG_MODE
    #'crawler.pipelines.MySQLPipeline': 200 if not DEBUG_MODE else None,
    'crawler.pipelines.SqlitePipeline': 200 if not DEBUG_MODE else None,
    #'crawler.pipelines.MySQLPipeline': 200,
}

# Downloader Middlewares
DOWNLOADER_MIDDLEWARES = {
    # default user-agent middleware
    # * disabled if we use proxies and random user-agents
    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': (
        400 if not PROXY_ENABLED else None
    ),
    # random user-agent middleware
    # * enabled only if we use proxies
    'crawler.middlewares.RotateUserAgentMiddleware': (
        400 if PROXY_ENABLED else None
    ),
    # proxy middleware
    # * enabled if PROXY_ENABLED and HTTP_PROXIES list is not empty
    'crawler.middlewares.ProxyMiddleware' : (
        740 if PROXY_ENABLED and HTTP_PROXIES else None
    ),
    'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 300,
}

# Spider Middlewares
#SPIDER_MIDDLEWARES = {}

# Extensions
#EXTENSIONS = {}

# Database settings
MYSQL_HOST = 'localhost'
MYSQL_PORT = 3306
MYSQL_USER = 'root'
# we keep MYSQL_PASSWORD here in case when the 'private_settings.py' module
# cannot be found (you don't have any private settings)
# or you don't need any special values for this settings
# (like when you have no password for mysql user at all)
# and such logic has to be applied to all such private settings
# that have default values (like an empty mysql password)
# and that are used (imported) in other modules (in this case - in 'db.py' module)
# otherwise we'll get 'ImportError' exceptions ('cannot import name ...')
# of course such settings that are remained for the compatibility
# should not contain any sensitive user data
# and if you have mysql password is set for the database
# it'll not work (if private_settings is missed)
# but you'll get a clear error (like: 'cannot connect to mysql db, wrong password')
# instead of unclear ImportError
MYSQL_PASSWORD = os.environ.get("MYSQL_PWD", '')
MYSQL_DB = 'bukalapak'

# sqlite DB:
PRODUCT_DB = "/data/python-scrapy-data-mining/product.db"

# Project settings
# List of bukalapak.com shops that will be processed
# if some shop cannot be found in DB, then its entry will be created
# shop's title will be extracted from its page and saved into DB automatically
# * use 'https' to avoid unnecessary redirects from 'http'
ShopUrls = []

# Private settings (sensitive user data)
try:
    # 'private_settings.py' module exists
    from private_settings import *
except ImportError:
    # 'private_settings.py' module doesn't exist
    pass

