from crawler.items import KeywordItem
from googleads import adwords
from keyword_setting import NumberOfKeywordToGeneratePerSeedKeyword
client = adwords.AdWordsClient.LoadFromStorage('googleads.yaml')

class GoogleAdApi():
  @classmethod
  def Test(cls):
    keywords = cls.GenerateRelatedKeywords('iphone case')
    print ("keywords: %s" % keywords)
    keywordItems = cls.GetAnalyticData(keywords)

  @classmethod
  def GenerateRelatedKeywords(cls, seedKeyword):
    assert type(seedKeyword) == type('')

    service = client.GetService('TargetingIdeaService', version='v201506')
    result = service.get( cls.FormTargetingIdeaSelector( [seedKeyword], dataToObtain = ['KEYWORD_TEXT'], requestType = 'IDEAS') )
    
    relatedKeywords = set()
    for entry in result['entries']:
      data = entry['data']
      keyword = data[0]
      try:
        _keyword = keyword['value']['value'].encode("utf-8")
        print ("_keyword: %s" % _keyword)
      except: 
        print 'something is wrong'
        continue

      relatedKeywords.add(_keyword)

    return list(relatedKeywords)

  @classmethod
  def GetAnalyticData( cls, seedKeyword ):
    assert type(keywords) == type([])
    keywords = list(set(keywords))
    assert len(keywords)
    service = client.GetService('TargetingIdeaService', version='v201506')

    selector =  cls.FormTargetingIdeaSelector( keywords, ['KEYWORD_TEXT', 'COMPETITION', 'SEARCH_VOLUME', 'AVERAGE_CPC'], requestType = 'STATS') 
    result = service.get(selector)
    
    if 'entries' not in result:
      print ("keywords: %s" % keywords)

    if 'entries' not in result:
      return []

    for entry in result['entries']:
      if 'data' not in entry:
        print entry
        print dir(entry)
        print keywords
      data = entry['data']

      keyword = data[0]
      competition = data[1]
      averageCpc = data[2]
      searchVolume = data[3]

      try:
        _keyword = keyword['value']['value'].encode("utf-8")
        print ("_keyword: %s" % _keyword)
      except: continue

      try:
        _competition = competition['value']['value']
        print ("_competition: %s" % _competition)
      except: continue

      try:
        _averageCpc = int(averageCpc['value']['value']['microAmount']/1000000)
        print ("_averageCpc: %s" % _averageCpc)
      except: continue

      try:
        _searchVolume = searchVolume['value']['value']
        print ("_searchVolume: %s" % _searchVolume)
      except: continue
      print

      Keywords = Keywords.filter(keyword = _keyword)
      assert Keywords.count() == 1

      Keywords[0]['keyword'] = _keyword
      Keywords[0]['competition'] = _competition 
      Keywords[0]['averageCpc'] = _averageCpc
      Keywords[0]['searchVolume'] = _searchVolume

  @classmethod
  def FormTargetingIdeaSelector(cls, keywords, dataToObtain, requestType = 'STATS'):
    '''
    keywords: e.g., ['iphone', 'smartphone']
    dataToObtain: e.g., ['KEYWORD_TEXT', 'COMPETITION', 'SEARCH_VOLUME', 'AVERAGE_CPC']
    '''
    assert type(keywords) == type([])
    assert len(keywords)
    paraTargetingIdeaSelector = {
        'searchParameters': 
          [
            {
              'xsi_type': 'LocationSearchParameter',
              'locations': [ {'id': 20436}, {'id': 20437}, {'id': 20439}, {'id': 20441}, {'id': 20443}, {'id': 20446}, {'id': 20450}, {'id': 20451}, {'id': 21291}, {'id': 9056637}, ], # indonesia cities
            },
            { 'xsi_type': 'LanguageSearchParameter', 'languages': [ {'id':1025}, ], },
            { 'xsi_type': 'RelatedToQuerySearchParameter', 'queries': keywords, },
          ],
        'ideaType': 'KEYWORD',
        'requestType':  requestType,
        'requestedAttributeTypes': dataToObtain,
        'paging': {
          'startIndex': str(0),
          'numberResults': str(NumberOfKeywordToGeneratePerSeedKeyword)
          },
        }
    return paraTargetingIdeaSelector 

if __name__ == '__main__':
  GoogleAdApi.Test()
