# -*- coding: utf-8 -*-

# class DefaultItemPipeline(object):

#  def process_item(self, item, spider):
#    return item

import logging.config
# logging.config.fileConfig('/home/xling/base/documents/scripts/programming/crossplatform/python/logging.conf')
import logging
logger = logging.getLogger(__name__)


class MySQLPipeline(object):

    def __init__(self):
        from . import db
        self.db = locals()['db']

    # def process_item(self, item, spider):
        # print "process_item", item
        #InsertOrUpdateItem(item, spider)

    def open_spider(self, spider):
        self.db.mysql_db.connect()

    def close_spider(self, spider):
        self.db.mysql_db.close()

    def process_item(self, item, spider):
        logger.debug("process_item:")
        logger.debug("type(item): %s" % type(item))
        logger.debug("str(item): %s" % str(item))

        if not getattr(item, '_db_model', None):
            assert 0
            return item
        if not (
            item._db_model in self.db.__dict__ and
            issubclass(self.db.__dict__[item._db_model], self.db.BaseModel)
        ):
            raise self.db.DBError('Undefined DB model :: %s' % item._db_model)
        item.__dict__['db_entry'] = None
        try:
            model = self.db.__dict__[item._db_model]
            if getattr(item, '_db_search_key', None):
                if item._db_search_key not in item:
                    raise Exception('Item has no value for :: _db_search_key')
                search_key_value = item[item._db_search_key]
                if not isinstance(search_key_value, basestring):
                    raise Exception('Unsupported :: _db_search_key')
                query = {}
                query[item._db_search_key] = search_key_value
                try:
                    db_entry = model.get(**query)
                    db_entry.__dict__['_data'].update(**item)
                    rows = db_entry.save()
                    # if the old item is the same as the new one, 0L is returned
                    #assert rows, rows
                except model.DoesNotExist:
                    db_entry = model.create(**item)
            else:
                db_entry = model(**item)
                ret = db_entry.save()
                # if the old item is the same as the new one, 0L is returned
                #assert rows, rows
            item.__dict__['db_entry'] = db_entry
        except Exception as e:
            spider.logger.warning(e.message)
            spider.logger.warning('DBError :: %s' % str(e))
            spider.logger.warning('item:: %s' % str(item))
            spider.logger.warning('type(item):: %s' % type(item))
            raise e

            import sys
            sys.exit(1)
        else:
            return item


class SqlitePipeline(object):

    def __init__(self):
        from . import product_db as db
        self.db = locals()['db']

    def process_item(self, item, spider):

        logger.debug("process_item:")
        logger.debug("type(item): %s" % type(item))
        logger.debug("str(item): %s" % str(item))

        try:

            product, created = self.db.Product.get_or_create(url=item["url"])
            if created:
                product.title=item["title"]
                product.category=item["category"]
                product.description=item["description"]
                product.save()

        except Exception as e:
            spider.logger.warning(e.message)
            spider.logger.warning('DBError :: %s' % str(e))
            spider.logger.warning('item:: %s' % str(item))
            spider.logger.warning('type(item):: %s' % type(item))
            raise e

            import sys
            sys.exit(1)
        else:
            return item
