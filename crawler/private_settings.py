# -*- coding: utf-8 -*-

# USER DEFINED PRIVATE SETTINGS
# * will not pushed to git-repository

import os
MYSQL_PASSWORD = os.environ.get("MYSQL_PWD", 'j0PEghIBmtW9JjVIUodf')

SHOPS = [
    'https://www.bukalapak.com/aksesorisgoprojakarta',
    'https://www.bukalapak.com/carcamera',
    'https://www.bukalapak.com/handphonecase',
    'https://www.bukalapak.com/tokosmartphone',
    'https://www.bukalapak.com/softcase',
    'https://www.bukalapak.com/xiaomicase', 
    'https://www.bukalapak.com/smartphonecase', 
    'https://www.bukalapak.com/aksesorihandphone', 
    'https://www.bukalapak.com/handphonecase',
    'https://www.bukalapak.com/softcase',
]

imageRootDir = '/tmp/imagesimilarity/'

NumberOfKeywordToGeneratePerSeedKeyword = 100
seedKeywords = [
  # manual slection for 4g
  'modem 4g',
  '4g lte',
  'modem lte',
  'mifi 4g',
  'huawei e5372s',
  'bolt orion',
  'bolt modem 4g',
  'harga modem 4g',
  'modem 4g terbaik',
  'internet 4g',
  'Huawei E3276',
  'Huawei E3372',
  'BOLT ZTE MF825A',
  'ZTE MF825A',
  'SIERRA 754S',
  '4G smartphone',
  'Kartu perdana 4G',
  'penguat sinyal 4G',
  '4G Modem',
  '4G Mifi',

  # manual slection for cctv camera
  'cctv online',
  'cctv murah',
  'video cctv',
  'kamera cctv murah',
  'cctv mini',
  'kamera tersembunyi',
  'harga cctv murah',
  'paket cctv murah',
  'jual cctv murah',
  'dvr cctv',

  # manual slection for smartwatch
  'smartwatch',
  'jam tangan android',
  'jam android',
  'smartwatch u8',
  'jam pintar',
  'android wear',
  'i watch',
  'pebble smartwatch',
  'jam tangan pintar',
  'u8 smartwatch',

  # from google keyword planner
  'bolt 4g',
  'redmi note',
  'bolt super 4g',
  'redmi note 4g',
  'modem wifi',
  'harga modem',
  'switch',
  'lte',
  '4g',
  '4g lte',
  'hot spot',
  'router wifi',
  'lg smartphone',
  'modem huawei',
  'modem gsm',
  'access point',
  'my bolt 4g',
  'wifi router',
  'modem 4g',
  'smartphone lg',
  'xiaomi 4g',
  'usb wifi',
  'samsung 4g',
  'smartphone 4g',
  'adsl',
  'modem adsl',

  # generate electronic words from Lazada
  'jual tanah',
  "Action Figure & Mainan Koleksi",
  "Aksesori Game",
  "Aksesori Komputer",
  "Aksesori TV & Video",
  "Aksesoris Audio",
  "Aksesoris Handphone",
  "Aksesoris Interior Mobil",
  "Aksesoris Kamera",
  "Aksesoris Mobil",
  "Aksesoris Musik",
  "Aksesoris Olahraga & Outdoor",
  "Aksesoris Tablet",
  "Aksesoris Travel",
  "Alas Meja & Aksesoris Dapur",
  "Alat Kesehatan dan Kecantikan",
  "Alat Tulis Kantor",
  "Anak Laki-Laki",
  "Anak Perempuan",
  "Audio Mini",
  "Audio Mobil/ Elektronik",
  "Audio",
  "Badminton",
  "Baju Muslim Pria",
  "Baju Muslim Wanita",
  "Basketball",
  "Baterai & Charger Handphone",
  "Baterai & Charger Tablet",
  "Bath & Body",
  "Blender, Mixer, & Penggiling",
  "Blu Ray & DVD Player",
  "Blu Ray Player",
  "Blue Lans",
  "Bluelans - Jam Tangan Trendi",
  "Board Game",
  "Bose",
  "Buku Lokal",
  "Buku Religi",
  "Buku",
  "Buku, Games & Musik",
  "CD Musik",
  "Casio",
  "Casual",
  "Citizen",
  "DSLR/SLR",
  "DVD Player",
  "Dekorasi Rumah",
  "Di Bawah Rp 189.000",
  "Docking Tablet",
  "Earset Bluetooth",
  "Electric Cigarette",
  "Elektronik Rumah Tangga",
  "Fancyqube Fashion Store - Mulai Dari Rp 9,000",
  "Fashion Anak Laki-Laki",
  "Fashion Anak Perempuan",
  "Fashion Outlet",
  "Fashion",
  "Film & Musik",
  "Film",
  "Filter",
  "Formal",
  "Furnitur",
  "Futsal",
  "GPS",
  "Gadget",
  "Game",
  "Gitar",
  "Gratis Ongkir Jabodetabek",
  "Groceries",
  "Guess",
  "Handphone & Tablet",
  "Handphone Basic",
  "Handphone",
  "Harddisk Eksternal",
  "Headphone Tablet",
  "Headphone",
  "Headset Handphone",
  "Helm & Pelindung",
  "Helm",
  "Highlights di Lazada",
  "Home Audio",
  "Home Beauty",
  "Infinix Hot Note",
  "Instrumen Musik",
  "Invicta",
  "Jacques Lemans",
  "Jam Tangan & Kacamata Olahraga",
  "Jam Tangan Anak",
  "Jam Tangan Pria",
  "Jam Tangan Wanita",
  "JamTangan",
  "KB & Alat Kontrasepsi",
  "Kabel Komputer",
  "Kacamata",
  "Kamera Drone",
  "Kamera Instan",
  "Kamera Lainnya",
  "Kamera Mini",
  "Kamera Mirrorless",
  "Kamera Pocket",
  "Kamera Prosumer",
  "Kamera Underwater",
  "Kamera Video Aksi",
  "Kamera Video",
  "Kamera",
  "Kartu Memori Handphone",
  "Kartu Memori",
  "Kecantikan",
  "Kereta Dorong & Perlengkapan Berkendara Bayi",
  "Kesehatan & Keamanan Bayi",
  "Kesehatan & Kebugaran",
  "Kesehatan & Kecantikan",
  "Keyboard",
  "Kids Dealz",
  "Kipas Angin",
  "Komponen Jaringan",
  "Komponen Komputer",
  "Kompor & Kompor Induksi",
  "Komputer & Laptop",
  "Komputer",
  "Konsol Game",
  "Koper Baju",
  "Koper Kabin",
  "Koper",
  "Korek Gas",
  "Laptop",
  "Lazada@Home",
  "Lemari Es",
  "Lensa",
  "Lifestyle",
  "MG Sport & Music",
  "MP3 Player & iPod",
  "Mainan & Bayi",
  "Mainan Edukasi",
  "Mainan Kendaraan & Remote Control",
  "Mainan Outdoor",
  "Mainan",
  "Makanan & Minuman",
  "Makeup",
  "Motor & Aksesoris",
  "Motor",
  "Mouse",
  "Nutrilon",
  "Nutrisi Olahraga",
  "Olahraga & Fitnes",
  "Olahraga & Outdoor",
  "Olahraga Individu",
  "Olahraga Tim",
  "Olay",
  "Organizer Pengepakan",
  "Otomotif",
  "Outdoor & Adventure",
  "Outdoor & Taman",
  "Oven & Microwave",
  "Pakaian Anak Laki-Laki",
  "Pakaian Anak Perempuan",
  "Pakaian Bayi",
  "Pakaian Pria",
  "Pakaian Wanita",
  "Panduan Belanja",
  "Parfum",
  "Pelindung Layar Handphone",
  "Pembersih Kamera",
  "Pembersih Rumah",
  "Pendingin Ruangan",
  "Penerangan",
  "Pengalih Jaringan",
  "Pengatur Berat Badan",
  "Penghisap Debu",
  "Pengunci Koper",
  "Penjernih Udara, Dehumidifier, & Humidifier",
  "Penyimpanan Dapur",
  "Peralatan Elektronik",
  "Peralatan Latihan Kardio",
  "Peralatan Mandi",
  "Peralatan Masak",
  "Peralatan Meja",
  "Peralatan Ranjang",
  "Peralatan Rumah Tangga Besar",
  "Peralatan Rumah Tangga",
  "Perangkat Pemanggang",
  "Perawatan Diri",
  "Perawatan Lantai",
  "Perawatan Mobil",
  "Perawatan Pakaian",
  "Perawatan Pria",
  "Perawatan Rambut",
  "Perawatan Rumah",
  "Perawatan Wajah",
  "Perhiasan",
  "Perlengkapan Anak",
  "Perlengkapan Bayi",
  "Perlengkapan Dapur & Ruang Makan",
  "Perlengkapan Dapur",
  "Perlengkapan Hewan Peliharaan",
  "Perlengkapan Kehamilan",
  "Perlengkapan Makan Bayi",
  "Perlengkapan Mandi Bayi",
  "Perlengkapan Medis",
  "Pewangi Mobil",
  "Philips Lighting",
  "Pisau Dapur",
  "Planet Sports",
  "Popok",
  "Premium",
  "Pria",
  "Printer & Tinta Printer",
  "Printer Multifungsi",
  "Printer",
  "Produk Terbaru Minggu Ini",
  "Produk Terbaru",
  "Promo",
  "Proyektor",
  "Pulsa Handphone",
  "Puzzle & Board Games",
  "Puzzle",
  "Rekreasi & Hiburan",
  "Rice Cooker, Steamer, & Selengkapnya",
  "Router",
  "Sampul Paspor",
  "Sarung & Pelindung Handphone",
  "Sarung & Pelindung Tablet",
  "Scanner",
  "Selengkapnya",
  "Sepakbola",
  "Sepatu & Pakaian Olahraga",
  "Sepatu Anak Laki-Laki",
  "Sepatu Anak Perempuan",
  "Sepatu Pria",
  "Sepatu Wanita",
  "Set Koper",
  "Setrika",
  "Shop By Brand",
  "Shop by Brand",
  "Smartphones",
  "Smartwatches",
  "Softwares",
  "Speaker Handphone",
  "Sports",
  "Steamer & Press Pakaian",
  "Suplemen Kecantikan",
  "Suplemen Kesehatan",
  "Suplemen Makanan",
  "Susu Formula Bayi",
  "Susu Pertumbuhan Formula",
  "Swiss Army",
  "TV & Video",
  "Tablet Dengan Telepon",
  "Tablet Tanpa Telepon",
  "Tablet",
  "Tas & Koper",
  "Tas & Pelindung Laptop",
  "Tas & Tas Ransel",
  "Tas Anak Laki-Laki",
  "Tas Anak Perempuan",
  "Tas Duffel Travel",
  "Tas Kamera",
  "Tas Laptop",
  "Tas Olahraga",
  "Tas Popok",
  "Tas Pria",
  "Tas Ransel",
  "Tas Sepatu",
  "Tas Wanita",
  "Televisi",
  "Tempat Penyimpanan",
  "Tenis",
  "This Week's Top Deals",
  "Timbangan Koper Digital",
  "Tinta Inkjet",
  "Toner Laser",
  "Tour & Travel",
  "Tripod",
  "Video Game",
  "Voucher & Layanan",
  "Voucher",
  "Wanita",
  "Watches Couple",
  "Wired Headset",
  "Yoga & Fitness",
  "Yoga",
  '3d tv',
  'Gratis Ongkir Jabodetabek',
  'Video Game',
  'aksesori game',
  'aksesori mp3 player',
  'aksesoris audio lainnya',
  'aksesoris audio',
  'aksesoris game lainnya',
  'aksesoris game',
  'aksesoris mp3 player',
  'aksesoris peralatan elektronik lainnya',
  'aksesoris peralatan eletronis',
  'aksesoris video',
  'amplifer',
  'amplifier',
  'audio mini lainnya',
  'audio mini',
  'audio',
  'av receiver',
  'blu-ray player',
  'boombox',
  'bracket dinding tv',
  'case game',
  'charger game',
  'dictaphone',
  'docking station',
  'dvd player portable',
  'dvd player stasioner',
  'dvd player',
  'earset bluetooth',
  'gadget lainnya',
  'game komputer',
  'game lainnya',
  'game',
  'handheld',
  'headphone in-ear',
  'headphone over-the-ear',
  'headphone pro & dj',
  'headphone',
  'headset game',
  'headset kabel',
  'home audio & theater',
  'home audio lainnya',
  'home audio',
  'home theater lainnya',
  'home theater sets',
  'home theater',
  'instrumen music',
  'internet add-on untuk tv',
  'ipod',
  'kabel mp3 player',
  'kabel tv',
  'kacamata video',
  'karaoke player',
  'karaoke',
  'kebel tv',
  'kesehatan & kebugaran',
  'keyboard game',
  'konsol game',
  'konsol genggam lainnya',
  'konsol stasioner',
  'layar proyektor',
  'mainan elekronik',
  'media player',
  'microfon',
  'mikrofon',
  'mouse game',
  'mp3 player & ipod',
  'mp3 player',
  'mp4 / video mp3 player',
  'nintendo 2ds',
  'nintendo 3ds',
  'nintendo ds',
  'nintendo wii u',
  'nintendo wii',
  'pelindung game',
  'pelindung layar mp3 player',
  'pelindung layar',
  'peralatan rekaman',
  'player portabel',
  'playstation',
  'promo pengiriman gratis tv',
  'proyektor mini',
  'proyektor',
  'ps 3',
  'ps vita',
  'ps4',
  'psp',
  'remote control',
  'rokok elektrik',
  'sarung & pelindung mp3 player',
  'sarung & pelindung',
  'set home theater',
  'sistem hi-fi',
  'smart tv',
  'smartwatch',
  'sound system panggung',
  'soundbar',
  'speaker bluetooth portabel',
  'speaker home theater',
  'speaker kabel portabel',
  'speaker portable ',
  'speaker portable bluetooth',
  'speaker portable kabel',
  'speaker portable',
  'speaker tv',
  'speaker',
  'stik & pengendali game',
  'stik & pengontrol game',
  'televisi smart 3d',
  'televisi',
  'tv 3d',
  'tv crt',
  'tv lcd',
  'tv led',
  'tv plasma',
  'tv smart 3d',
  'video',
  'xbox 360',
  'xbox one',
  ]

