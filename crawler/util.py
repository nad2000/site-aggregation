class Util():
  @classmethod
  def GetValueByXpaths(cls, logger, response, xpaths):
    try:
      assert type(xpaths) == type([]), 'xpaths should be a list'
      for xpath in xpaths:
        if len(response.xpath(xpath)):
          return response.xpath(xpath)[0].extract()
        else:
          continue
      else:
        # logger.warning("cannot parse the xpath. response.url: %s" % response.url  + (", \n xpaths: %s" %  xpaths))
        return '-1'
    except Exception as e:
      import sys
      logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      logger.error('str(e) %s' %  str(e))
      logger.debug("response.url: %s" % response.url)
      logger.debug("xpaths: %s" % xpaths)
      logger.error(e.message)
      raise e

  # @classmethod
  # def UpdateProduct(cls, logger, response, productUrl, shopReference, xpaths):
    # '''
    # MetricsItem need its product exists in DB before it can be inserted into DB.
    # This method directly writes Product into DB instead of throwing ProductItem into pipelines so that MetricsItem can be immediately inserted.
    # '''
    # from crawler.db import Product
    # from crawler.settings import DEBUG_MODE

    # cls.logger = logger
    # cls.logger.debug("UpdateProduct")
    # from peewee import IntegrityError
    # try:
      # assert 'product_title' in xpaths
      # assert 'product_category' in xpaths
      # assert 'product_description' in xpaths

      # product = Product()
      # products = Product().select().where(Product.url == productUrl) 
      # if products.count() > 0: 
        # product = products[0]

      # # title returned from xpath usually contains \n and the head and tail.
      # title = Util.GetValueByXpaths(cls.logger, response, xpaths['product_title'])
      # if title[0] == '\n': 
        # title = title[1:]
      # if title[-1] == '\n': 
        # title = title[:-1]
      # product.title = title

      # product.category = Util.GetValueByXpaths(cls.logger, response, xpaths['product_category'])
      # product.description = Util.GetValueByXpaths(cls.logger, response, xpaths['product_description'])
      # product.url = productUrl
      # if shopReference:
        # product.shop = shopReference
      # if not DEBUG_MODE:
        # product.save()
      # return product
    # except IntegrityError, message:
      # errorcode = message[0]  # get MySQL error code
      # if errorcode == 1062:  # if duplicate 
        # pass
    # except Exception as e:
      # import sys
      # cls.logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
      # cls.logger.error('DBError :: %s' %  str(e))
      # cls.logger.debug("productUrl: %s" % productUrl)
      # cls.logger.error(e.message)
      # raise e
