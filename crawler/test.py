import peewee
db = peewee.MySQLDatabase("db_name", host="localhost", user="root", passwd="xxxx")

def table(model):
  model.create_table(fail_silently=True)
  return model

class MySQLModel(peewee.Model):
  class Meta:
    database = db

@table
class ShopUrl(MySQLModel):
  url = peewee.CharField()

db.connect()

shopUrls = [ 'https://www.bukalapak.com/satya_yogaswara', ]

if __name__ == '__main__':
  for shopUrlStr in shopUrls:
    shopUrl = ShopUrl()
    shopUrl.url = shopUrlStr
    shopUrl.save()
