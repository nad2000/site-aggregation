# -*- coding: utf-8 -*-

# Items support default values
# default values can be present as:
# (1) - any python type (supported by DB if we're going to store item there)
# (2) - a callable object (like function)
# in this case (2) default value will be populated by the result of function's execution
# ** at the moment when an item will be populated by a spider
# for example, we bind the default value of 'timestamp' to 'utcnow' function of datetime module,
# so every time when a spider will populate 'MetricsItem' instance with scraped data,
# this function will be called and the returned value will be saved into 'timestamp' field
# (it'll be UTC timestamp of the moment when a spider will populate this item)
# so we can deligate some spider's tasks (like getting current time) to 'Items' logic
# "default value" isn't a feature of Scrapy (it was some time ago, but not anymore)
# so it's implemented via 'DefaultItemLoader' class from 'itemloader' module
#
# By default Scrapy always works with lists of items values
# so when item is populated and passed to some database middleware
# it'll be present as a list (even if you use 'extract_first' method of selector)
# and most of the time we need items fields to be present as a strings, not lists
# so we should convert such lists to strings by getting first value from them
# over and over again...
# to save time and make work with Scrapy more simple
# we add 'take-first' behavior (implemented via item loaders - itemloaders.py module)
# in this case the first real non-empty string value of some field
# will be extracted automatically by items loader
# but sometimes (very rarely though) we need fields value to be exactly a list
# to get it - just add 'take_first=False' argument to any field you want to be a list
#
# '_db_model' is a special field
# it sets what DB model (table) from 'db' module
# will be used to store item's data
#
# '_db_search_key' also is a special field
# it indicates what field (db column) with unique index will be used
# to get data from DB for some item
# index must be unique, otherwise this behavior has no sense
# for example, URL field (db column) is always unique
# if two database entries (rows) have equal URL value (page, product etc.)
# then it's the same entity
# db search key is used to retrieve data from database
# by using field's value with exactly such name ('url' in our case)
# otherwise we have no way to find already saved entries
# and correctly work with them

from datetime import datetime
from scrapy import Item, Field
from scrapy.loader.processors import MapCompose
import sys

# input filter (processor)


def is_price(value):
    return is_digit(
        map(lambda v: v.replace('.', '', 1), value)
    )

# input filter (processor)


def is_digit(value):
    return filter(lambda v: v.isdigit(), value)

# ITEMS:


class ShopItem(Item):
    # DB model (table)
    _db_model = 'Shop'
    # DB search key (unique index)
    _db_search_key = 'url'
    # fields:
    title = Field()
    url = Field()
    feedbackNumber = Field(default=0)
    score = Field(default=1.0)
    lastVisited = Field(default=datetime.utcnow)


class ProductItem(Item):
    # DB model (table)
    _db_model = 'Product'
    # DB search key (unique index)
    _db_search_key = 'url'
    # fields:
    url = Field()
    title = Field()
    category = Field()
    description = Field()
    imagePath = Field(default='')
    similarity = Field(default='0.0')

    shop = Field()

# timestamps will be saved in UTC time (GMT+0)
# to save all timestamps in local time (server's timezone)
# change 'utcnow' function to 'now' (datetime.now)


class MetricsItem(Item):
    # DB model (table)
    _db_model = 'Metrics'
    # DB search key (not used for this item/model)
    _db_search_key = None
    # fields:
    timestamp = Field(default=datetime.utcnow)
    price = Field(
        default=0,
        input_processor=is_price,
        output_processor=MapCompose(int),
    )
    # stock = Field(
    #    input_processor=is_digit,
    #    output_processor=MapCompose(int),
    #)
    pageviews = Field(
        default=0,
        input_processor=is_digit,
        output_processor=MapCompose(int),
    )
    likes = Field(
        # default=0,
        input_processor=is_digit,
        output_processor=MapCompose(int),
    )
    sales = Field(
        default=0,
        input_processor=is_digit,
        output_processor=MapCompose(int),
    )
    commentNumber = Field(
        default=0
    )
    score = Field(
        default=0
    )
    product = Field()


class KeywordItem(Item):
    _db_model = 'Keywords'
    _db_search_key = 'keyword'

    seedKeyword = Field()
    seedExpander = Field()
    keyword = Field()
    searchVolume = Field()
    averageCpc = Field()
    competition = Field()
    productNumber = Field()


class SearchKeyItem(Item):
    _db_model = 'SearchKey'
    #_db_search_key = 'searchKey'
    _db_search_key = None

    searchKey = Field()
    subSearchKey = Field()
    product = Field()

from crawler.itemloader import DefaultItemLoader
from crawler.db import SearchKey


def RecordProduct(logger, response, url, title, imagePath, price, searchKey, subSearchKey):
    logger.debug("RecordProduct")

    # record the product itself
    try:
        productItem = DefaultItemLoader(ProductItem(), response=response)
        productItem.add_value('url',  url)
        productItem.add_value('title',  title)
        productItem.add_value('imagePath',  imagePath)
        productItem = productItem.load_item()
        logger.debug("yield productItem: %s" % str(productItem))
    except Exception as e:
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        logger.error(e.message)
        raise e
    yield productItem

    # record price of the first returned product
    try:
        metricsItem = DefaultItemLoader(MetricsItem(), selector=response)
        # price must be string
        metricsItem.add_value('price', str(price))
        '''
    If productItem has an url that has existed in db. productItem will be 
    automatically replaced with the productItem existing in DB.
    '''
        metricsItem.add_value('product',  productItem.db_entry)
        metricsItem = metricsItem.load_item()
        logger.debug("yield metricsItem: %s" % str(metricsItem))
    except Exception as e:
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        logger.error(e.message)
        raise e
    yield metricsItem

    # record SearckKeyItem for the lazada product
    if SearchKey.select().where(SearchKey.searchKey == searchKey, SearchKey.product == productItem.db_entry).count() > 0:
        return
        yield

    try:
        searchKeyItem = DefaultItemLoader(SearchKeyItem(), response=response)
        searchKeyItem.add_value('searchKey', searchKey)
        searchKeyItem.add_value('subSearchKey', subSearchKey)
        searchKeyItem.add_value('product', productItem.db_entry)
        searchKeyItem = searchKeyItem.load_item()
        logger.debug("yield searchKeyItem: %s" % str(searchKeyItem))
    except Exception as e:
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        logger.error(e.message)
        raise e
    yield searchKeyItem
