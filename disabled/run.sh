#!/bin/bash

echo "Exporting project's root dir to the system PYTHONPATH..."
PROJECT_ROOT=$( cd $(dirname $0) ; pwd -P )
export PYTHONPATH=$PROJECT_ROOT
echo 'PROJECT_ROOT::'$PROJECT_ROOT

echo "Changing a current working dir to the project's root dir"
cd $PROJECT_ROOT

echo 'Activating virtualenv...'
source venv/bin/activate
